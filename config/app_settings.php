<?php

return [

    // All the sections for the settings page
    'sections' => [
        'app' => [
            'title' => 'Impostazioni generali.',
            'descriptions' => '', // (optional)
            'icon' => 'fa fa-cog', // (optional)

            'inputs' => [
                [
                    'name' => 'show_empty_shops', // unique key for setting
                    'type' => 'checkbox', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Mostra negozi senza offerte nelle applicazioni', // label for input
                    // optional properties
                    'placeholder' => 'Mostra negozi senza offerte nelle applicazioni', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => '', //required|min:2|max:20 validation rules for this input
                    'value' => '1', // any default value
                    'hint' => 'Selezionandolo gli utenti visualizzeranno anche i negozi senza alcuna offerta' // help block text for input
                ],
                [
                    'name' => 'default_commerciant_coupons', // unique key for setting
                    'type' => 'number', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'Numero offerte acquistate iniziali', // label for input
                    // optional properties
                    'placeholder' => 'Numero offerte acquistate iniziali', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => '', //required|min:2|max:20 validation rules for this input
                    'value' => '100', // any default value
                    'hint' => 'Questo valore è il numero di offerte che il commerciante potrà pubblicare quando registrato' // help block text for input
                ],
                [
                    'name' => 'enabled_api_keys', // unique key for setting
                    'type' => 'text', // type of input can be text, number, textarea, select, boolean, checkbox etc.
                    'label' => 'API KEY abilitate, SEPARARE con ; (punto e virgola)', // label for input
                    // optional properties
                    'placeholder' => 'API KEY (>= 8 caratteri) separate da ;', // placeholder for input
                    'class' => 'form-control', // override global input_class
                    'style' => '', // any inline styles
                    'rules' => '', //required|min:2|max:20 validation rules for this input
                    'value' => '', // any default value
                    'hint' => 'Le API key sono semplici stringhe che forniscono accesso a soggetti terzi a certe funzionalità delle API' // help block text for input
                ]
            ]
        ]
    ],

    // Setting page url, will be used for get and post request
    'url' => 'settings',

    // Any middleware you want to run on above route
    'middleware' => ['auth'],

    // View settings
    'setting_page_view' => 'settings',
    'flash_partial' => 'app_settings::_flash',

    // Setting section class setting
    'section_class' => 'card mb-3',
    'section_heading_class' => 'card-header',
    'section_body_class' => 'card-body',

    // Input wrapper and group class setting
    'input_wrapper_class' => 'form-group',
    'input_class' => 'form-control',
    'input_error_class' => 'has-error',
    'input_invalid_class' => 'is-invalid',
    'input_hint_class' => 'form-text text-muted',
    'input_error_feedback_class' => 'text-danger',

    // Submit button
    'submit_btn_text' => 'Salva impostazioni',
    'submit_success_message' => 'Impostazioni salvate correttamente.',

    // Remove any setting which declaration removed later from sections
    'remove_abandoned_settings' => false,

    // Controller to show and handle save setting
    'controller' => '\QCod\AppSettings\Controllers\AppSettingController',
];
