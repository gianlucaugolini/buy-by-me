<?php

return [
    'options' => [
        'options_pagination' => '15',
        'options_date_format' => 'd-m-Y H:i:s',
    ]
];
