<?php

return [

    'path'          => env('CROSS_STORAGE_PATH', 'public/'),
    'remote_path'   => env('IMAGES_URL', 'storage/'),

];
