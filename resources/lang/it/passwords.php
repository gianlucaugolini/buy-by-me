<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'La password deve essere lunga almeno di 8 caratteri.',
    'reset' => 'La tua password è state resettata!',
    'sent' => 'Ti abbiamo inviato un\'email di reset password!',
    'token' => 'Token di reset password non valido.',
    'user' => "Non è stato possibile trovare un utente con l'email inserita.",

];
