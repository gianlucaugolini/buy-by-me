@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.offers_trade_page.offers_trade_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_offer_trade' )</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="offer_trade_form" method="post" action="{{ route( 'offers-trade-save' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-md-5 col-lg-4 mx-auto">
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md-6 text-center">
                                            <div>
                                                <label for="c_offers_bought">
                                                    @lang( 'labels.offers_trade_page.offers_bought' )
                                                </label>
                                            </div>
                                            <div>
                                                {{ Form::input( 'text', 'c_offers_bought', $commerciant->c_offers_bought, [ 'class' => 'form-control bbm-num' ] ) }}
                                            </div>
                                        </div>
                                        <div class="form-group col-12 col-md-6 text-center">
                                            <div>
                                                <label for="c_offers_used">
                                                    @lang( 'labels.offers_trade_page.offers_used' )
                                                </label>
                                            </div>
                                            <div>
                                                {{ Form::input( 'text', 'c_offers_used', $commerciant->c_offers_used, [ 'class' => 'form-control bbm-num' ] ) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="commerciant_id" type="hidden" value="{{ $commerciant->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.offers_trade_page.submit' )
                                    </button>
                                    <a href="{{ route( 'user-details', [ 'id' => $commerciant->user_id ] ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
