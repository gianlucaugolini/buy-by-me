@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">

                        @switch( $user->type_id )
                            @case( 1 )
                            <a href="{{ route( 'administrators' ) }}">AMMINISTRATORI</a>
                            @break;
                            @case( 2 )
                                <a href="{{ route( 'customers' ) }}">CLIENTI</a>
                            @break;
                            @case( 3 )
                                 <a href="{{ route( 'shop-owners' ) }}">COMMERCIANTI</a>
                            @break;
                            @case( 4 )
                                <a href="{{ route( 'supervisors' ) }}">SUPERVISORI</a>
                            @break;
                        @endswitch </a> > "{{ $user->fullname }}"
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.user_details_page.fullname' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $user->fullname }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.user_details_page.email' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <a href="mailto:{{ $user->email }}?subject=BuyByMe">
                                        {{ $user->email }}
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.user_details_page.created_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $user->created_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.user_details_page.updated_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $user->updated_at ) }}
                                </div>
                            </div>
                            @switch( $user->type_id )
                                @case( 3 )
                                <div class="row">
                                    <div class="col">
                                        <strong>
                                            @lang( 'labels.user_details_page.api_key' )
                                        </strong>
                                    </div>
                                    <div class="col">
                                        <p class="{{ $user->api_id ? "bg-warning" : "" }}">{{ $user->api_id ? $user->api_id : "-" }}</p>
                                    </div>
                                </div>
                                @break;
                            @endswitch

                        </div>
                        <div class="col-12 col-md-6">
                            @switch( $user->type_id )
                                @case( 1 )
                                    @include( 'users.details-partials.details-administrator', [ 'data' => $user_details ] )
                                    @break;
                                @case( 2 )
                                    @include( 'users.details-partials.details-customer', [ 'data' => $user_details ] )
                                    @break;
                                @case( 3 )
                                    @include( 'users.details-partials.details-commerciant', [ 'data' => $user_details ] )
                                    @break;
                                @case( 4 )
                                    @include( 'users.details-partials.details-supervisor', [ 'data' => $user_details ] )
                                    @break;
                            @endswitch
                        </div>
                    </div>
                    @switch( $user->type_id )
                        @case( 2 )
                            @include( 'users.details-partials.preferences-customer', [ 'data' => $user_details ] )
                            @break;
                        @default
                            @break;
                    @endswitch
                </div>

                <div class="card-footer text-center">

                    @switch( $user->type_id )
                        @case( 1 )
                            @include( 'users.details-partials.actions-admin', [ 'data' => $user ] )
                            <a href="{{ route( 'administrators' ) }}" class="btn btn-primary">
                                @lang( 'labels.back_to_list_label' )
                            </a>
                            @break;
                        @case( 2 )
                            @include( 'users.details-partials.actions-customer', [ 'data' => $user_details ] )
                            <a href="{{ route( 'customers' ) }}" class="btn btn-primary">
                                @lang( 'labels.back_to_list_label' )
                            </a>
                            @break;
                        @case( 3 )
                            @include( 'users.details-partials.actions-commerciant', [ 'data' => $user_details ] )
                            <a href="{{ route( 'shop-owners' ) }}" class="btn btn-primary">
                                @lang( 'labels.back_to_list_label' )
                            </a>
                            @break;
                        @case( 4 )
                            @include( 'users.details-partials.actions-supervisor', [ 'data' => $user_details ] )
                            <a href="{{ route( 'supervisors' ) }}" class="btn btn-primary">
                                @lang( 'labels.back_to_list_label' )
                            </a>
                            @break;
                    @endswitch
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
