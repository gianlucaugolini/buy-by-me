@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Modifica: {{ $user->fullname }}
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_owners')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_customer_form" method="post" action="{{ route( 'update-shop-owner' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.user_edit.first_name' )
                                    </label>
                                    {{ Form::input( 'text', 'first_name', $commerciant_details->c_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="last_name">
                                        @lang( 'labels.user_edit.last_name' )
                                    </label>
                                    {{ Form::input( 'text', 'last_name', $commerciant_details->c_last_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.user_edit.email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', $user->email, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="c_subscription">
                                        @lang( 'labels.user_edit.subscription_expiration' )
                                    </label>
                                    <div class="input-group date" id="c-subscription" data-target-input="nearest">
                                        {{ Form::input( 'text', 'c_subscription', $commerciant_details->c_subscription ? \Carbon\Carbon::createFromTimestamp( $commerciant_details->c_subscription ) : '', [ 'class' => 'form-control datetimepicker-input', 'data-target' => '#c-subscription' ] ) }}
                                        <div class="input-group-append" data-target="#c-subscription" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="user_id" type="hidden" value="{{ $user->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.user_edit.submit' )
                                    </button>
                                    <button onclick="location.href='{{ url()->previous() }}'" class="btn btn-warning">
                                        @lang( 'labels.annulla' )
                                    </button>
                                     <a href="{{ route( 'shop-owners') }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section( 'scripts' )
    <script type="text/javascript">
        $(function () {

            $('#c-subscription').datetimepicker({
                format: 'Y/MM/DD HH:mm:ss',
                useCurrent: false,
                showTodayButton: true
            });

        });
    </script>
@endsection
