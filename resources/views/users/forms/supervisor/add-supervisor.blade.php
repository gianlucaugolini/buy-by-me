@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.user_add.add_supervisor_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.new_desc_supervisors')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_supervisor_form" method="post" action="{{ route( 'create-supervisor' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.user_add.first_name' )
                                    </label>
                                    {{ Form::input( 'text', 'first_name', old( 'first_name' ), [ 'class' => 'form-control','required'  ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="last_name">
                                        @lang( 'labels.user_add.last_name' )
                                    </label>
                                    {{ Form::input( 'text', 'last_name', old( 'last_name' ), [ 'class' => 'form-control','required'  ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.user_add.email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', old( 'email' ), [ 'class' => 'form-control','required'  ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="password">
                                        @lang( 'labels.user_add.password' )
                                    </label>
                                    {{ Form::input( 'password', 'password', '', [ 'class' => 'form-control','required' ] ) }}
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col-sm">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-row">
                                                <div class="form-group col-12">
                                                    <label for="s_address">
                                                        @lang( 'labels.user_edit.supervisor_edit_cc_ext' )
                                                    </label>
                                                    {{ Form::select( 'cc_ext', $cc_list, '', [ 'class' => 'form-control' ] ) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.user_add.submit' )
                                    </button>
                                    <a href="{{ route( 'supervisors' ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
