@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.user_edit.supervisor_edit_form_title', [ 'fullname' => $user->fullname ] )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_supervisors')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_supervisor_form" method="post" action="{{ route( 'update-supervisor' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.user_edit.first_name' )
                                    </label>
                                    {{ Form::input( 'text', 'first_name', $supervisor_details->ccs_name, [ 'class' => 'form-control','required'] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="last_name">
                                        @lang( 'labels.user_edit.last_name' )
                                    </label>
                                    {{ Form::input( 'text', 'last_name', $supervisor_details->ccs_last_name, [ 'class' => 'form-control','required' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.user_edit.email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', $user->email, [ 'class' => 'form-control','required' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="password">
                                        @lang( 'labels.user_edit.password' )
                                    </label>
                                    {{ Form::input( 'password', 'password', '', [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if( !$supervisor_details->catena )
                <div class="row mb-2">
                    <div class="col-sm">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-12">
                                        <label for="s_address">
                                            @lang( 'labels.user_edit.supervisor_edit_cc_ext' )
                                        </label>
                                        {{ Form::select( 'cc_ext', $cc_list, old( 'cc_ext' ), [ 'class' => 'form-control' ] ) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row mb-2">
                    <div class="col-sm">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label for="first_name">
                                            @lang( 'labels.user_edit.supervisor_edit_cc_ext' )
                                        </label>
                                        <input type="hidden" name="cc_ext" value="{{ $supervisor_details->catena->id }}">
                                        @lang( 'labels.supervisor.supervisor_of' ) <a href="{{ route( 'mall-details', [ 'id' => $supervisor_details->catena->id ] ) }}"> {{ $supervisor_details->catena->cc_name }}</a> <a href="{{ route( 'remove-from-mall', [ 'id' => $user->id ] ) }}" class="btn btn-danger ml-3">Rimuovi da supervisione</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="user_id" type="hidden" value="{{ $user->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.user_edit.submit' )
                                    </button>
                                    <button onclick="location.href='{{ url()->previous() }}'" class="btn btn-warning">
                                        @lang( 'labels.annulla' )
                                    </button>
                                    <a href="{{ route( 'user-details', [ 'id' => $user->id ]) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
