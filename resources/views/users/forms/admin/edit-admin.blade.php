@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.user_add.add_admin_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_administrators')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_admin_form" method="post" action="{{ route( 'update-admin' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="fullname">
                                        @lang( 'labels.user_edit.fullname' )
                                    </label>
                                    {{ Form::input( 'text', 'fullname', $user->fullname, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.user_edit.email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', $user->email, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="user_id" type="hidden" value="{{ $user->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.user_edit.submit' )
                                    </button>
                                    <a href="{{ route( 'administrators' ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
