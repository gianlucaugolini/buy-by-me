@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Modifica: {{ $user->fullname }}
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_customers')</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <form name="edit_customer_form" method="post" action="{{ route( 'update-customer' ) }}">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.user_edit.first_name' )
                                    </label>
                                    {{ Form::input( 'text', 'first_name', $customer_details->u_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="last_name">
                                        @lang( 'labels.user_edit.last_name' )
                                    </label>
                                    {{ Form::input( 'text', 'last_name', $customer_details->u_last_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.user_edit.email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', $user->email, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="start_date">
                                        @lang( 'labels.user_edit.prov' )
                                    </label>
                                    {{ Form::select('prov', App\Models\Provincia::pluck('prov','prov'),$customer_details->u_prov,[ 'class' => 'form-control', 'placeholder' => 'Seleziona una provincia...' ])}}
                                </div>
                                <div class="form-group col">
                                    <label for="end_date">
                                        @lang( 'labels.user_edit.prov_code' )
                                    </label>
                                    {{ Form::select('prov_code', App\Models\Provincia::pluck('prov_code','prov_code'),$customer_details->u_prov_code,[ 'class' => 'form-control', 'placeholder' => 'Seleziona un codice provincia...' ])}}

                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="user_id" type="hidden" value="{{ $user->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.user_edit.submit' )
                                    </button>
                                    <div onclick="location.href='{{ url()->previous() }}'" class="btn btn-warning">
                                        @lang( 'labels.annulla' )
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
