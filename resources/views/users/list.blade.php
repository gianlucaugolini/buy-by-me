@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.messages' )
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            {{ $title }}
                            @if( \Route::current()->getName() === 'customers' ||  \Route::current()->getName() === 'customers-search' ||  \Route::current()->getName() === 'commerciants-search')
                                - {{$filterLabel}} ({{$users->count()}})
                            @endif
                            @if( \Route::current()->getName() === 'shop-owners')
                                - ({{$users->count()}})
                            @endif
                        </h4>
                    </div>
                    @if( \Route::current()->getName() === 'customers' ||  \Route::current()->getName() === 'customers-search')
                    <div class="">
                        <a href="{{ route( 'export-customers') }}" class="text-center list-group-item list-group-item-action bg-warning">
                            <i class="fa fa-file-download"></i>EXPORT CSV
                        </a>
                    </div>
                    @endif
                    @if( \Route::current()->getName() === 'shop-owners' ||  \Route::current()->getName() === 'commerciants-search')
                        <div class="">
                            <a href="{{ route( 'export-shop-owner') }}" class="text-center list-group-item list-group-item-action bg-warning">
                                <i class="fa fa-file-download"></i>EXPORT CSV
                            </a>
                        </div>
                    @endif
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if( \Route::current()->getName() === 'customers' ||  \Route::current()->getName() === 'customers-search' ||  \Route::current()->getName() === 'commerciants-search' ||  \Route::current()->getName() === 'shop-owners')
                            <div class="text-left">
                                <form class="form-inline" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="searchUser" id="searchUser" placeholder="Cerca utente"
                                        @if( !empty($search))
                                         value="{{$search}}"
                                        @endif
                                        >
                                    </div>
                                    <button type="submit" class="btn btn-primary mb-2">Cerca</button>
                                </form>

                            </div>
                            <br>
                            <div class="text-center">
                                Ordina: Nome <a href="{{url()->current()}}?order=fullname">ASC</a> | <a href="{{url()->current()}}?order=fullname&desc=1">DESC</a> - Email <a href="{{url()->current()}}?order=email">ASC</a> | <a href="{{url()->current()}}?order=email&desc=1">DESC</a> - Creazione <a href="{{url()->current()}}?order=created_at">ASC</a> | <a href="{{url()->current()}}?order=created_at&desc=1">DESC</a>
                            </div>
                            <div class="text-right">
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Filtra
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                            @if( \Route::current()->getName() === 'customers' ||  \Route::current()->getName() === 'customers-search')
                                                <a class="dropdown-item" href="{{ route('customers')}}">Tutti</a>
                                                <a class="dropdown-item" href="{{ route('customers', ['filter' => 'blocked'])}}">Bloccati</a>
                                                <a class="dropdown-item" href="{{ route('customers', ['filter' => 'confirmed'])}}">Confermati</a>
                                            @elseif( \Route::current()->getName() === 'commerciants-search' ||  \Route::current()->getName() === 'shop-owners')
                                                <a class="dropdown-item" href="{{ route('shop-owners')}}">Tutti</a>
                                                <a class="dropdown-item" href="{{ route('shop-owners', ['filter' => 'blocked'])}}">Bloccati</a>
                                                <a class="dropdown-item" href="{{ route('shop-owners', ['filter' => 'confirmed'])}}">Confermati</a>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        @endif
                        <div class="list-group">
                            @if (\Route::current()->getName() !== 'supervisors')
                                <a class="list-group-item list-group-item-action flex-column align-items-center">
                                    <div class="d-flex w-100 justify-content-between">
                                        <p>Anagrafica</p>
                                        <p>E-Mail</p>
                                        <p>Data creazione</p>
                                        @if (\Route::current()->getName() === 'shop-owners' ||  \Route::current()->getName() === 'commerciants-search')
                                            <p>Scadenza abbonamento</p>
                                        @endif
                                        <p>Informazioni</p>
                                    </div>
                                </a>
                            @endif
                            @foreach( $users as $u => $user )
                                @if( \Route::current()->getName() === 'customers' ||  \Route::current()->getName() === 'customers-search')
                                     <a href="{{ route( 'user-details', [ 'id' => $user->id ] ) }}" class="list-group-item list-group-item-action flex-column align-items-start" >
                                        <div class="d-flex w-100 justify-content-between">
                                            <p>{{ $user->fullname }}</p>
                                            <p>{{ $user->email }}</p>
                                            <p>{{ date( 'd/m/Y H:i:s',$user->created_at)}}</p>
                                            <p>
                                                @if( $user->u_blocked === 1 )
                                                    <i class="fa fa-ban" aria-label="Utente bloccato" data-toggle="tooltip"  data-placement="right" title="Utente bloccato" style="font-size:20px;color:red;"></i>
                                                @endif
                                                @if( $user->u_confirmed === 1 )
                                                    <i class="fa fa-check-circle" aria-label="Utente confermato via email" data-toggle="tooltip"  data-placement="right" title="Utente confermato via email" style="font-size:20px;color:green;"></i>
                                                @endif
                                            </p>
                                        </div>
                                     </a>
                                @elseif (\Route::current()->getName() === 'supervisors')
                                     <a href="{{ route( 'user-details', [ 'id' => $user->user->id ] ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip" data-placement="right" title="click per visualizzare i dettagli del Supervisore">{{ $user->user->fullname }}</a>
                                    @if ($user->catena)
                                       <a data-toggle="tooltip" data-placement="right" title="click per visualizzare i dettagli della Catena Commerciale" href="{{ route( 'mall-details', [ 'id' => $user->catena->id ] ) }}" class="btn btn-primary btn-sm ml-2">
                                            Catena commerciale: {{$user->catena->cc_name}}
                                        </a>
                                    @else
                                        <span class="btn btn-warning btn-sm ml-2">Nessuna catena commerciale.</span><br>
                                    @endif
                                <br>
                                @elseif (\Route::current()->getName() === 'shop-owners' ||  \Route::current()->getName() === 'commerciants-search')
                                        <a href="{{ route( 'user-details', [ 'id' => $user->id ] ) }}" class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="d-flex w-100 justify-content-between">
                                                <p>{{ $user->fullname }}</p>
                                                <p>{{ $user->email }}</p>
                                                <p>{{ date( 'd/m/Y H:i:s',$user->created_at)}}</p>
                                                <p>{{ $user->c_subscription != null ? $user->c_subscription : "Abbonamento ILLIMITATO" }}</p>
                                                <p>
                                                    @if( $user->c_blocked === 1 )
                                                        <i class="fa fa-ban" aria-label="Utente bloccato" data-toggle="tooltip"  data-placement="right" title="Utente bloccato" style="font-size:20px;color:red;"></i>
                                                    @endif
                                                    @if( $user->c_confirmed === 1 )
                                                        <i class="fa fa-check-circle" aria-label="Utente confermato via email" data-toggle="tooltip"  data-placement="right" title="Utente confermato via email" style="font-size:20px;color:green;"></i>
                                                    @endif
                                                        @if( $user->api_id)
                                                            <i class="fa fa-key" aria-label="Utente creato tramite API KEY" data-toggle="tooltip"  data-placement="right" title="Utente creato tramite API KEY" style="font-size:20px;color:orange;"></i>
                                                        @endif
                                                </p>
                                                </div>
                                        </a>
                                @elseif (\Route::current()->getName() === 'administrators')
                                    <a href="{{ route( 'user-details', [ 'id' => $user->id ] ) }}" class="list-group-item list-group-item-action flex-column align-items-start" >
                                        <div class="d-flex w-100 justify-content-between">
                                            <p>{{ $user->fullname }}</p>
                                        </div>
                                    </a>
                                @endif

                            @endforeach
                        </div>
                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $users->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>


                    <div class="card-footer text-center">
                        @if( \Route::current()->getName() === 'administrators' )
                            <a href="{{ route( 'add-admin' ) }}" class="btn btn-primary">
                                @lang( 'labels.user_add.add_new_admin' )
                            </a>
                        @elseif( \Route::current()->getName() === 'supervisors' )
                            <a href="{{ route( 'add-supervisor' ) }}" class="btn btn-primary">
                                @lang( 'labels.user_add.add_new_supervisor' )
                            </a>
                        @endif
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
