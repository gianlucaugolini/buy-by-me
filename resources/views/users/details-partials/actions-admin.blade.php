<a href="{{ route( 'edit-admin', [ 'id' => $data->id ] ) }}" class="btn btn-primary">
    @lang( 'labels.user_details_page.actions.edit_admin' )
</a>
