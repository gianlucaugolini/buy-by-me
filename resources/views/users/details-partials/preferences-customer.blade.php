<div class="row">
    <div class="col">
        <hr>
    </div>
</div>

<div class="row">
    <div class="col">
        <div class="row">
            <div class="col-12 p-3 mb-2 bg-light text-dark text-center h5">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_title' )
                </strong>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_ios_token' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                {{ $data->u_notification_ios_token }}
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_android_token' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                {{ $data->u_notification_android_token }}
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_ios_on' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                @if($data->u_notification_ios_on === 1)
                   ABILITATE
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_android_on' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                @if($data->u_notification_android_on === 1)
                    ABILITATE
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_provincia' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                {{ $data->u_notification_provincia }}
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_only_filters' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                    @if($data->u_notification_only_filters === 1)
                        ABILITATE
                    @else
                        -
                    @endif
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_discounts' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                <?php
                    $discountsSelected = explode(";",$data->u_notification_discounts);

                    foreach($discountsSelected as $key=>$value) {
                        $discountsSelected[$key] = $value == 1 ? ((1+$key)*10)."%"  : "";
                    }
                    echo implode (", ", array_filter($discountsSelected)); //" (".$data->u_notification_discounts.")";
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <strong>
                    @lang( 'labels.user_details_page.preferences.notification_categories' )
                </strong>
            </div>
            <div class="col-12 col-md-6">
                <?php

                $categories = ["Vestiario",
                    "Alimentari",
                    "Elettronica",
                    "Viaggi",
                    "Gioielli",
                    "Casa",
                    "Bellezza",
                    "Giochi",
                    "Divertimento",
                    "Ristoranti",
                    "Farmacie",
                    "Salute",
                    "Professionisti",
                    "Tecnici",
                    "Varie"];

                $categoriesNotificationsSelected = explode(";",$data->u_notification_categories);

                foreach($categoriesNotificationsSelected as $key=>$value) {
                    $categoriesNotificationsSelected[$key] = $value == 1 ? $categories[$key]  : "";
                }
                echo implode (", ", array_filter($categoriesNotificationsSelected)); //" (".$data->u_notification_categories.")";
                ?>
            </div>
        </div>
    </div>
</div>
