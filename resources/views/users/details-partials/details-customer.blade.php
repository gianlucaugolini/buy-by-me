<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.first_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->u_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.last_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->u_last_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.prov' )
        </strong>
    </div>
    <div class="col">
        {{ $data->u_prov }} - {{ $data->u_prov_code }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.blocked' )
        </strong>
    </div>
    <div class="col">
        <p class="{{ $data->blocked_label == "NO" ? "" : "bg-danger" }}">{{ $data->blocked_label }}</p>
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.confirmed' )
        </strong>
    </div>
    <div class="col">
        {{ $data->confirmed_label }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.token' )
        </strong>
    </div>
    <div class="col">
        {{ $data->u_token }}
    </div>
</div>
