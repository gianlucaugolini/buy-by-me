<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.first_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->ccs_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.last_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->ccs_last_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.blocked' )
        </strong>
    </div>
    <div class="col">
        {{ $data->blocked_label }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.cc_ext' )
        </strong>
    </div>
    <div class="col">
        @if ($data->catena)
            <a href="{{ route( 'mall-details', [ 'id' => $data->catena->id ] ) }}">
                {{ $data->catena ? $data->catena->cc_name : '------' }}
            </a>
        @else
            {{ $data->catena ? $data->catena->cc_name : '------' }}
        @endif


    </div>
</div>
