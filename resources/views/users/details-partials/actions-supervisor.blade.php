@if( $data->ccs_blocked === 0 )
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 1 ] ) }}" class="btn btn-danger mb-1">
        @lang( 'labels.user_details_page.actions.block_action' )
    </a>
@else
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 0 ] ) }}" class="btn btn-warning mb-1">
        @lang( 'labels.user_details_page.actions.unblock_action' )
    </a>
@endif

<a href="{{ route( 'edit-supervisor', [ 'id' => $data->user_id ] ) }}" class="btn btn-primary mb-1">
    @lang( 'labels.user_details_page.actions.edit_supervisor' )
</a>

@if( $data->catena )
    <a href="{{ route( 'mall-details', [ 'id' => $data->catena->id ] ) }}" class="btn btn-primary mb-1">
        @lang( 'labels.user_details_page.actions.view_mall' )
    </a>
@endif

<button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
    Cancella tutti i dati
</button>
<div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h5 class="modal-title text-center" id="deleteDataModalLabel">
                        @lang( 'labels.supervisor_deletion.delete_supervisor_title', [ 'supervisor_name' => $data->ccs_name  . " ".$data->ccs_last_name] )
                    </h5>
                </div>
            </div>

            <div class="modal-body">
                @lang( 'labels.supervisor_deletion.delete_supervisor_alert' )
            </div>
            <div class="modal-footer">
                <div class="col-12 text-center">
                    <a href="{{ route( 'delete-supervisor-data', [ 'id' => $data->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.supervisor_deletion.confirm_supervisor_deletion' )
                    </a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>
</div>

