<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.first_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->c_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.last_name' )
        </strong>
    </div>
    <div class="col">
        {{ $data->c_last_name }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.blocked' )
        </strong>
    </div>
    <div class="col">
        {{ $data->blocked_label }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.confirmed' )
        </strong>
    </div>
    <div class="col">
        {{ $data->confirmed_label }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.token' )
        </strong>
    </div>
    <div class="col">
        {{ $data->c_token }}
    </div>
</div>
<div class="row">
    <div class="col">
        <strong>
            @lang( 'labels.user_details_page.sub_expiration' )
        </strong>
    </div>
    <div class="col">
        {{ is_null($data->c_subscription) ? __('labels.user_details_page.expiration_null') : date( 'd-m-Y H:i:s', $data->c_subscription ) }}
    </div>
</div>
