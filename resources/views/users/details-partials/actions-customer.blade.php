@if( $data->u_blocked === 0 )
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 1 ] ) }}" class="btn btn-danger">
        @lang( 'labels.user_details_page.actions.block_action' )
    </a>
@else
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 0 ] ) }}" class="btn btn-warning">
        @lang( 'labels.user_details_page.actions.unblock_action' )
    </a>
@endif

@if( $data->u_confirmed === 0 )
    <a href="{{ route( 'user-confirmed', [ 'id' => $data->user_id, 'confirmed' => 1 ] ) }}" class="btn btn-success">
        @lang( 'labels.user_details_page.actions.confirm_action' )
    </a>
@else
    <a href="{{ route( 'user-confirmed', [ 'id' => $data->user_id, 'confirmed' => 0 ] ) }}" class="btn btn-warning">
        @lang( 'labels.user_details_page.actions.unconfirm_action' )
    </a>
@endif

<a href="{{ route( 'edit-customer', [ 'id' => $data->user_id ] ) }}" class="btn btn-primary">
    @lang( 'labels.user_details_page.actions.edit_customer' )
</a>

<a href="{{ route( 'customer-favs', [ 'id' => $data->user_id ] ) }}" class="btn btn-primary">
    @lang( 'labels.user_details_page.actions.customer_favs' )
</a>

<button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
    Cancella tutti i dati
</button>
<div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h5 class="modal-title text-center" id="deleteDataModalLabel">
                        @lang( 'labels.consumer_deletion.delete_consumer_title', [ 'consumer_name' => $data->u_name  . " ".$data->u_last_name] )
                    </h5>
                </div>
            </div>

            <div class="modal-body">
                @lang( 'labels.consumer_deletion.delete_consumer_alert' )
            </div>
            <div class="modal-footer">
                <div class="col-12 text-center">
                    <a href="{{ route( 'delete-customer', [ 'id' => $data->user_id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.consumer_deletion.confirm_consumer_deletion' )
                    </a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>
</div>

