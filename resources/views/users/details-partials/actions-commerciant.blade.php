@if( $data->c_blocked === 0 )
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 1 ] ) }}" class="btn btn-danger mb-1">
        @lang( 'labels.user_details_page.actions.block_action' )
    </a>
@else
    <a href="{{ route( 'user-status', [ 'id' => $data->user_id, 'status' => 0 ] ) }}" class="btn btn-warning mb-1">
        @lang( 'labels.user_details_page.actions.unblock_action' )
    </a>
@endif

@if( $data->c_confirmed === 0 )
    <a href="{{ route( 'user-confirmed', [ 'id' => $data->user_id, 'confirmed' => 1 ] ) }}" class="btn btn-success mb-1">
        @lang( 'labels.user_details_page.actions.confirm_action' )
    </a>
@else
    <a href="{{ route( 'user-confirmed', [ 'id' => $data->user_id, 'confirmed' => 0 ] ) }}" class="btn btn-warning mb-1">
        @lang( 'labels.user_details_page.actions.unconfirm_action' )
    </a>
@endif

<a href="{{ route( 'edit-shop-owner', [ 'id' => $data->user_id ] ) }}" class="btn btn-primary mb-1">
    @lang( 'labels.user_details_page.actions.edit_commerciant' )
</a>

<a href="{{ route( 'shop-details', [ 'id' => $data->shop->id ] ) }}" class="btn btn-primary mb-1">
    @lang( 'labels.user_details_page.actions.view_shop' )
</a>

<a href="{{ route( 'offers-trade', [ 'id' => $data->id ] ) }}" style="display: none" class="btn btn-primary mb-1">
    @lang( 'labels.user_details_page.actions.offers_trade' )
</a>


<button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
    Cancella tutti i dati
</button>

<div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h5 class="modal-title text-center" id="deleteDataModalLabel">
                        @lang( 'labels.user_details_page.actions.delete_commerciant_title', [ 'commerciant_name' => $data->c_name . ' ' . $data->c_last_name  ] )
                    </h5>
                </div>
            </div>

            <div class="modal-body">
                @lang( 'labels.user_details_page.actions.delete_commerciant_alert' )
            </div>
            <div class="modal-footer">
                <div class="col-12 text-center">
                    <a href="{{ route( 'delete-shop-owner-data', [ 'id' => $data->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.user_details_page.actions.confirm_commerciant_deletion' )
                    </a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>
</div>
