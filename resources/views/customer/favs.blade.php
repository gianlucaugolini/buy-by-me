@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        {{ $title }}
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <div class="list-group">
                                @foreach( $customer_favs as $cf => $customer_fav )
                                    <div class="text-center list-group-item">
                                        <a href="{{ route( 'offers-details', [ 'id' => $customer_fav->offer->id ] ) }}">
                                            {{ $customer_fav->offer->o_title }}
                                        </a> (di: <a href="{{ route( 'shop-details', [ 'id' => $customer_fav->offer->shop->id ] ) }}">
                                            {{ $customer_fav->offer->shop->s_name}})
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col">
                            {{ $customer_favs->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <a href="{{ route( 'user-details', [ 'id' => $user->id ]) }}" class="btn btn-primary">
                        @lang( 'labels.user_details_page.actions.back_to_customer_details_label' )
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
