@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.messages' )
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            @lang( 'labels.home_title' )
                        </h4>
                    </div>

                    <div class="card-body">
                            @include('app_settings::_settings')
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-4">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            @lang( 'labels.settings_download_title' )
                        </h4>
                    </div>

                    <div class="card-body text-center">
                        <a href="{{route('db_backup')}}"> @lang( 'labels.settings_download_backup' )</a>
                    </div>

                    <div class="card-body text-center">
                        <a href="{{route('db_partial_backup_csv')}}"> @lang( 'labels.settings_download_partial_csv_backup' )</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
