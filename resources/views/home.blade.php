@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        @lang( 'labels.home_title' )
                    </h4>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="list-group">
                        <a href="{{ route( 'administrators' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip" data-placement="right" title=" @lang( 'labels.home_desc_administrators' )" >
                            Amministratori
                        </a>
                        <a href="{{ route( 'customers' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip" data-placement="right" title=" @lang( 'labels.home_desc_customers' )" >
                            Clienti
                        </a>
                        <a href="{{ route( 'shop-owners' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip" data-placement="right" title=" @lang( 'labels.home_desc_owners' )" >
                            Commercianti
                        </a>
                        <a href="{{ route( 'supervisors' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_supervisors' )" >
                            Supervisori catene commerciali
                        </a>
                        <a href="{{ route( 'all-shops' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_shops' )" >
                            Negozi
                        </a>
                        <a href="{{ route( 'all-malls' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_malls' )" >
                            Catene commerciali
                        </a>
                        <a href="{{ route( 'all-centers' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_centers' )" >
                            Centri Commerciali
                        </a>
                        <a href="{{ route( 'all-offers' ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_offers' )" >
                            Offerte
                        </a>
                        <a href="/public/settings" class="text-center list-group-item list-group-item-action" data-toggle="tooltip"  data-placement="right" title=" @lang( 'labels.home_desc_impostazioni' )" >
                            Impostazioni
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
