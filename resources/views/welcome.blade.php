@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body text-center">
                        <div>
                            <img src="{{ url( '/images/Icon-60@3x.png' ) }}" />
                        </div>
                        <h1>
                            @lang( 'labels.welcome_message' )
                        </h1>

                        <!-- Authentication Links -->
                        @guest
                            <a class="btn btn-primary" href="{{ route('login') }}">{{ __('Login') }}</a>
                        @else
                            <a class="btn btn-primary" href="{{ route('home') }}">{{ __('Home') }}</a>
                        @endguest

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
