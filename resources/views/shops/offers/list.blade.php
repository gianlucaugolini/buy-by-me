@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            {{ $title }}
                        </h4>
                    </div>

                    <div class="card-body">
                        @if ( session( 'status' ) )
                            <div class="alert alert-success" role="alert">
                                {{ session( 'status' ) }}
                            </div>
                        @endif
                        <div class="list-group">
                            @if ( count($offers)==0 )
                                @lang( 'labels.offer_list_page.offers_unavailable')
                            @endif

                            @foreach( $offers as $o => $offer )
                                <a href="{{ route( 'offers-details', [ 'id' => $offer->id ] ) }}" class="text-center list-group-item list-group-item-action {{ $offer->o_blocked === 0 ? "bg-success" : "bg-warning" }}">
                                    {{ $offer->o_title }}
                                    <br>
                                    (cat: {{$offer->categoria->name}} creata il: {{date( 'd/m/Y H:i:s', $offer->created_at )}}) Views: {{$offer->o_views}} Likes: {{$offer->o_likes}}
                                    @if( $offer->o_blocked === 1 )
                                        <i class="fa fa-ban" aria-label="Offerta bloccata" data-toggle="tooltip"  data-placement="right" title="Offerta bloccata" style="font-size:20px;color:red;"></i>
                                    @endif
                                    @if( $offer->o_cancelled != null )
                                        <i class="fa fa-window-close" aria-label="Offerta annullata" data-toggle="tooltip"  data-placement="right" title="Offerta annullata" style="font-size:20px;color:red;"></i>
                                    @endif
                                </a>
                            @endforeach
                        </div>

                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $offers->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
