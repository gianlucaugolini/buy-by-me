@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.messages' )
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            {{ $title }}
                        </h4>
                    </div>
                    <div class="">
                        <a href="{{ route( 'export-shops') }}" class="text-center list-group-item list-group-item-action bg-warning">
                            <i class="fa fa-file-download"></i>EXPORT CSV
                        </a>
                    </div>
                    <br>
                    <div class="text-center">
                        Ordina: Nome <a href="{{url()->current()}}?order=s_name">ASC</a> | <a href="{{url()->current()}}?order=s_name&desc=1">DESC</a> - Creazione <a href="{{url()->current()}}?order=created_at">ASC</a> | <a href="{{url()->current()}}?order=created_at&desc=1">DESC</a>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="list-group">
                            @forelse ($shops as $s => $shop )
                                <a href="{{ route( 'shop-details', [ 'id' => $shop->id ] ) }}" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="tooltip"  data-placement="right" title="">

                                    <div class="d-flex w-100 justify-content-between">
                                    <p>
                                    <strong>{{ $shop->s_name }}</strong>
                                    </p>
                                    <p>
                                    Creazione: {{ date( 'd/m/Y H:i:s',$shop->created_at)}}
                                    </p>
                                    <p>
                                       Ultima modifica: {{ date( 'd/m/Y H:i:s',$shop->updated_at)}}
                                    </p>
                                    <p>
                                    @if( $shop->s_blocked === 1 )
                                        <i class="fa fa-ban" aria-label="Negozio bloccato" data-toggle="tooltip"  data-placement="right" title="Negozio bloccato" style="font-size:20px;color:red;"></i>
                                    @endif
                                    </p>
                                    <p>
                                    @if ($shop->api_id)
                                        <i class="fa fa-key" style="font-size:20px;color:orange;" title="Generato da API KEY"></i>
                                    @endif
                                    </p>
                                    </div>
                                </a>
                            @empty
                                <li class="text-center list-group-item list-group-item-action">
                                    Nessun shop associato a questo centro commerciale
                                </li>
                            @endforelse
                        </div>

                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $shops->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route( 'add-shop' ) }}" class="btn btn-primary">
                            @lang( 'labels.shop_add_new' )
                        </a>
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
