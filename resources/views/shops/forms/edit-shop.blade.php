@extends('layouts.app')

@section('extra_headers')
    <!-- Map -->
    <link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css" type="text/css">
    <style>
        .map {
            height: 400px;
            width: 100%;
        }
    </style>
    <script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
@endsection

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            Modifica: {{ $shop->s_name }}
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_shops')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_shop_form" method="post" action="{{ route( 'update-shop' ) }}" class="shop-form">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="s_name">
                                        @lang( 'labels.shop_edit_page.shop_name' )
                                    </label>
                                    {{ Form::input( 'text', 's_name', $shop->s_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12">
                                    <label for="s_desc">
                                        @lang( 'labels.shop_edit_page.shop_description' )
                                    </label>
                                    {{ Form::textarea( 's_desc', $shop->s_desc, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_web">
                                        @lang( 'labels.shop_edit_page.shop_phone' )
                                    </label>
                                    {{ Form::input( 'text', 's_phone', $shop->s_phone, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_phone">
                                        @lang( 'labels.shop_edit_page.shop_web' )
                                    </label>
                                    {{ Form::input( 'text', 's_web', $shop->s_web, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12">
                                    <label for="s_prd">
                                        @lang( 'labels.shop_edit_page.shop_products' )
                                    </label>
                                    {{ Form::input( 'text', 's_prd', $shop->s_prd, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <div>
                                        <label for="s_cat">
                                            @lang( 'labels.shop_edit_page.shop_cat' )
                                        </label>
                                    </div>
                                    @foreach( $categories as $c => $category )
                                        <div class="custom-control custom-switch">
                                            {{ Form::radio( 's_cat', $category->id, $category->id === $shop->s_cat, [ 'class' => 'custom-control-input', 'id' => 'sc-' . $category->id ] ) }}
                                            <label class="custom-control-label" for="sc-{{ $category->id }}">
                                                {{ $category->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_address">
                                        @lang( 'labels.shop_edit_page.shop_address' )
                                    </label>
                                    {{ Form::input( 'text', 's_address', $shop->s_address, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_city">
                                        @lang( 'labels.shop_edit_page.shop_city' )
                                    </label>
                                    {{ Form::input( 'text', 's_city', $shop->s_city, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_prov">
                                        @lang( 'labels.shop_edit_page.shop_prov' )
                                    </label>
                                    {{ Form::select('s_prov', App\Models\Provincia::pluck('prov','prov'),$shop->s_prov,[ 'class' => 'form-control', 'placeholder' => 'Seleziona una provincia...' ])}}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_prov_code">
                                        @lang( 'labels.shop_edit_page.shop_prov_code' )
                                    </label>
                                    {{ Form::select('s_prov_code', App\Models\Provincia::pluck('prov_code','prov_code'),$shop->s_prov_code ,[ 'class' => 'form-control', 'placeholder' => 'Seleziona un codice di provincia...' ])}}
                                </div>
                                <div class="form-group col-12 col-md-2">
                                    <label for="s_cap">
                                        @lang( 'labels.shop_edit_page.shop_cap' )
                                    </label>
                                    {{ Form::input( 'text', 's_cap', $shop->s_cap, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-5">
                                    <label for="s_lat">
                                        @lang( 'labels.shop_edit_page.shop_lat' )
                                    </label>
                                    {{ Form::input( 'text', 's_lat', $shop->s_lat, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-5">
                                    <label for="s_lon">
                                        @lang( 'labels.shop_edit_page.shop_lon' )
                                    </label>
                                    {{ Form::input( 'text', 's_lon', $shop->s_lon, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div id="map" class="map"></div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="s_address">
                                        @lang( 'labels.shop_edit_page.shop_catena_commerciale_ext' )
                                    </label>
                                    {{ Form::select( 's_catena_commerciale_ext', $cc_list, $shop->s_catena_commerciale_ext, [ 'class' => 'form-control', 'placeholder' => 'Seleziona una Catena Commerciale...' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-9">
                                    <label for="s_centro_commerciale">
                                        @lang( 'labels.shop_edit_page.shop_centro_commerciale' )
                                    </label>
                                    {{ Form::select( 's_centro_commerciale_ext', $c_list, $shop->s_centro_commerciale_ext, [ 'class' => 'form-control', 'placeholder' => 'Selezionare una Centro Commerciale' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-3">
                                    <label for="s_centro_commerciale_piano">
                                        @lang( 'labels.shop_edit_page.shop_centro_commerciale_piano' )
                                    </label>
                                    {{ Form::input( 'text', 's_centro_commerciale_piano', $shop->s_centro_commerciale_piano, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="shop_id" type="hidden" value="{{ $shop->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.shop_edit_page.submit' )
                                    </button>
                                    <button onclick="location.href='{{ url()->previous() }}'" class="btn btn-warning">
                                        @lang( 'labels.annulla' )
                                    </button>
                                    <a href="{{ route( 'shop-details', [ 'id' => $shop->id ]) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

        var map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([{{ $shop->s_lon }}, {{ $shop->s_lat }}]),
                zoom: 17
            })
        });

        add_map_point({{ $shop->s_lon }},{{ $shop->s_lat }});

        function add_map_point(lng, lat) {
            var vectorLayer = new ol.layer.Vector({
                source:new ol.source.Vector({
                    features: [new ol.Feature({
                        geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lng), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857')),
                    })]
                }),
                style: new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: [0.5, 0.5],
                        anchorXUnits: "fraction",
                        anchorYUnits: "fraction",
                        src: "/public/images/poi.png"
                    })
                })
            });
            map.addLayer(vectorLayer);
        }
    </script>
@endsection
