@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.shop_add_page.shop_add_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.new_desc_shops')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_shop_form" method="post" action="{{ route( 'create-shop' ) }}" class="shop-form">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.shop_add_page.commerciant_first_name' )
                                    </label>
                                    {{ Form::input( 'text', 'first_name', old( 'first_name' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="last_name">
                                        @lang( 'labels.shop_add_page.commerciant_last_name' )
                                    </label>
                                    {{ Form::input( 'text', 'last_name', old( 'last_name' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="email">
                                        @lang( 'labels.shop_add_page.commerciant_email' )
                                    </label>
                                    {{ Form::input( 'email', 'email', old( 'email' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col">
                                    <label for="subscription">
                                        @lang( 'labels.shop_add_page.subscription_expiration' )
                                    </label>
                                    <div class="input-group date" id="subscription" data-target-input="nearest">
                                        {{ Form::input( 'text', 'subscription', '', [ 'class' => 'form-control datetimepicker-input', 'data-target' => '#subscription' ] ) }}
                                        <div class="input-group-append" data-target="#subscription" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="s_name">
                                        @lang( 'labels.shop_add_page.shop_name' )
                                    </label>
                                    {{ Form::input( 'text', 's_name', old( 's_name' ) , [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12">
                                    <label for="s_desc">
                                        @lang( 'labels.shop_add_page.shop_description' )
                                    </label>
                                    {{ Form::textarea( 's_desc', old( 's_desc' ) , [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_web">
                                        @lang( 'labels.shop_add_page.shop_phone' )
                                    </label>
                                    {{ Form::input( 'text', 's_phone', old( 's_phone' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_phone">
                                        @lang( 'labels.shop_add_page.shop_web' )
                                    </label>
                                    {{ Form::input( 'text', 's_web', old( 's_web' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12">
                                    <label for="s_prd">
                                        @lang( 'labels.shop_add_page.shop_products' )
                                    </label>
                                    {{ Form::input( 'text', 's_prd', old( 's_prd' ) , [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <div>
                                        <label for="s_cat">
                                            @lang( 'labels.shop_add_page.shop_cat' )
                                        </label>
                                    </div>
                                    @foreach( $categories as $c => $category )
                                        <div class="custom-control custom-switch">
                                            {{ Form::radio( 's_cat', $category->id, $category->id === old( 's_cat' ), [ 'class' => 'custom-control-input', 'id' => 'sc-' . $category->id ] ) }}
                                            <label class="custom-control-label" for="sc-{{ $category->id }}">
                                                {{ $category->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_address">
                                        @lang( 'labels.shop_add_page.shop_address' )
                                    </label>
                                    {{ Form::input( 'text', 's_address', old( 's_address' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_city">
                                        @lang( 'labels.shop_add_page.shop_city' )
                                    </label>
                                    {{ Form::input( 'text', 's_city', old( 's_city' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_prov">
                                        @lang( 'labels.shop_add_page.shop_prov' )
                                    </label>
                                    {{ Form::select('s_prov', App\Models\Provincia::pluck('prov','prov'),old( 's_prov' ),[ 'class' => 'form-control', 'placeholder' => 'Seleziona una provincia...' ])}}
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="s_prov_code">
                                        @lang( 'labels.shop_add_page.shop_prov_code' )
                                    </label>
                                    {{ Form::select('s_prov_code', App\Models\Provincia::pluck('prov_code','prov_code'),old( 's_prov_code' ),[ 'class' => 'form-control', 'placeholder' => 'Seleziona un codice di provincia...' ])}}
                                </div>
                                <div class="form-group col-12 col-md-2">
                                    <label for="s_cap">
                                        @lang( 'labels.shop_add_page.shop_cap' )
                                    </label>
                                    {{ Form::input( 'text', 's_cap', old( 's_cap' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-5">
                                    <label for="s_lat">
                                        @lang( 'labels.shop_add_page.shop_lat' )
                                    </label>
                                    {{ Form::input( 'text', 's_lat', old( 's_lat' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-5">
                                    <label for="s_lon">
                                        @lang( 'labels.shop_add_page.shop_lon' )
                                    </label>
                                    {{ Form::input( 'text', 's_lon', old( 's_lon' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="shop_catena_commerciale_ext">
                                        @lang( 'labels.shop_add_page.shop_catena_commerciale_ext' )
                                    </label>
                                    {{ Form::select( 's_catena_commerciale_ext', $cc_list, old( 's_catena_commerciale_ext' ), [ 'class' => 'form-control', 'placeholder' => 'Selezionare una Catena Commerciale' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-9">
                                    <label for="s_centro_commerciale">
                                        @lang( 'labels.shop_add_page.shop_centro_commerciale' )
                                    </label>
                                    {{ Form::select( 's_centro_commerciale_ext', $c_list, old( 's_centro_commerciale_ext' ), [ 'class' => 'form-control', 'placeholder' => 'Selezionare una Centro Commerciale' ] ) }}
                                </div>
                                <div class="form-group col-12 col-md-3">
                                    <label for="s_centro_commerciale_piano">
                                        @lang( 'labels.shop_add_page.shop_centro_commerciale_piano' )
                                    </label>
                                    {{ Form::input( 'text', 's_centro_commerciale_piano', old( 's_centro_commerciale_piano' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.shop_add_page.submit' )
                                    </button>
                                    <a href="{{ route( 'all-shops' ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section( 'scripts' )
    <script type="text/javascript">
        $(function () {

            $('#subscription').datetimepicker({
                format: 'Y/MM/DD HH:mm:ss',
                useCurrent: false,
                showTodayButton: true
            });

        });
    </script>
@endsection
