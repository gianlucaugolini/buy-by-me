@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.shop_add_page.shop_add_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_shop_image')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_shop_image" method="post" action="{{ route( 'shop-save-image' ) }}" enctype="multipart/form-data">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.shop_add_image_page.shop_image' )
                                    </label>
                                    {{ Form::file( 'shop_image', old( 'shop_image' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="shop_id" type="hidden" value="{{ $shop->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.shop_add_image_page.submit' )
                                    </button>
                                    <a href="{{ route( 'shop-details', [ 'id' => $shop->id ] ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
