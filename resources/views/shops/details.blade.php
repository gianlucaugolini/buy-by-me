@extends('layouts.app')
@section('extra_headers')
    <!-- Map -->
    <link href="{{ asset('css/ol.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css" type="text/css">
    <style>
        .map {
            height: 400px;
            width: 100%;
        }
    </style>
    <script src="{{ asset('js/ol.js') }}"></script>
@endsection
@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        <a href="javascript:window.history.back()">NEGOZI</a> >
                        {{ $title }} "{{ $shop->s_name }}"
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_image' )
                                    </strong>
                                </div>
                                <div class="col-12">
                                    <div>
                                        @if( $shop->s_image )
                                            <img class="img-fluid" src="{{ config( 'cross-storage.remote_path' ) . $shop->s_image }}" alt="" />
                                            <br>
                                            <a href="{{ route( 'shop-upload-image', [ 'id' => $shop->id ] )  }}" class="btn btn-danger btn-sm">Modifica Immagine Negozio</a>
                                        @else
                                            <a href="{{ route( 'shop-upload-image', [ 'id' => $shop->id ] )  }}" class="btn btn-primary btn-sm">Aggiungi Immagine Negozio</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_name' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_name }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_cat' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->categoria ? $shop->categoria->name : '-----' }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_phone' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_phone !== '' ? $shop->s_phone : '-----' }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_web' )
                                    </strong>
                                </div>
                                <div class="col">
                                    @if( $shop->s_web !== '' )
                                    <a href="{{$shop->s_web}}" target="_blank">{{$shop->s_web}}</a>
                                    @else
                                        -------
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.created_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $shop->created_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.updated_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $shop->updated_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.blocked' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <p class="{{ $shop->s_blocked === 0 ? "" : "bg-danger" }}">{{ $shop->blocked_label }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.api_key' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <p class="{{ $shop->api_id ? "bg-warning" : "" }}">{{ $shop->api_id ? $shop->api_id : "-" }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_description' )
                                    </strong>
                                </div>
                                <div class="col-12">
                                    {{ $shop->s_desc }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <br>
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_products' )
                                    </strong>
                                </div>
                                <div class="col-12">
                                    {{ $shop->s_prd }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-4">

                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_address' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_address }}
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-4">

                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_cap' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_cap }}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_city' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_city }}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_prov' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_prov}}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_prov_code' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_prov_code}}
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col">
                                            <strong>
                                                @lang( 'labels.shop_details_page.shop_lat' )
                                            </strong>
                                        </div>
                                        <div class="col">
                                            {{ $shop->s_lat }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="row">
                                        <div class="col">
                                            <strong>
                                                @lang( 'labels.shop_details_page.shop_lon' )
                                            </strong>
                                        </div>
                                        <div class="col">
                                            {{ $shop->s_lon }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col">
                                    <div id="map" class="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_catena_commerciale_ext' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <a href="{{ route( 'mall-details', [ 'id' => $shop->catena ] ) }}" class="">
                                        {{ $shop->catena ? $shop->catena->cc_name : '--' }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_centro_commerciale' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <a href="{{ route( 'center-details', [ 'id' => $shop->centroCommerciale ] ) }}" class="">
                                        {{ $shop->centroCommerciale ? $shop->centroCommerciale->cc_name : '------' }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.shop_details_page.shop_centro_commerciale_piano' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $shop->s_centro_commerciale_piano }}
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="card-footer text-center">
                    <a href="{{ route( 'all-shops' ) }}" class="btn btn-primary mb-1">
                        @lang( 'labels.back_to_list_label' )
                    </a>

                    @if( $shop->s_blocked === 0 )
                        <a href="{{ route( 'shop-status', [ 'id' => $shop->id, 'status' => 1 ] ) }}" class="btn btn-danger mb-1">
                            @lang( 'labels.shop_details_page.actions.block_action' )
                        </a>
                    @else
                        <a href="{{ route( 'shop-status', [ 'id' => $shop->id, 'status' => 0 ] ) }}" class="btn btn-warning mb-1">
                            @lang( 'labels.shop_details_page.actions.unblock_action' )
                        </a>
                    @endif

                    <a href="{{ route( 'edit-shop', [ 'id' => $shop->id ] ) }}" class="btn btn-primary mb-1">
                        @lang( 'labels.shop_details_page.actions.edit_shop' )
                    </a>

                    <a href="{{ route( 'user-details', [ 'id' => $shop->owner->user_id ] ) }}" class="btn btn-primary mb-1">
                        @lang( 'labels.shop_details_page.actions.shop_owner' )
                    </a>

                    <a href="{{ route( 'shop-offers', [ 'id' => $shop->id ] ) }}" class="btn btn-primary mb-1">
                        @lang( 'labels.shop_details_page.actions.shop_offers' )
                    </a>


                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
                        Cancella tutti i dati
                    </button>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h5 class="modal-title text-center" id="deleteDataModalLabel">
                        @lang( 'labels.shop_deletion.delete_shop_title', [ 'shop_name' => $shop->s_name ] )
                    </h5>
                </div>
            </div>

            <div class="modal-body">
                @lang( 'labels.shop_deletion.delete_shop_alert' )
            </div>
            <div class="modal-footer">
                <div class="col-12 text-center">
                    <a href="{{ route( 'delete-shop-data', [ 'id' => $shop->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.shop_deletion.confirm_shop_deletion' )
                    </a>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">

        var map = new ol.Map({
            target: 'map',
            layers: [
                new ol.layer.Tile({
                    source: new ol.source.OSM()
                })
            ],
            view: new ol.View({
                center: ol.proj.fromLonLat([{{ $shop->s_lon }}, {{ $shop->s_lat }}]),
                zoom: 17
            })
        });

        add_map_point({{ $shop->s_lon }},{{ $shop->s_lat }});

        function add_map_point(lng, lat) {
            var vectorLayer = new ol.layer.Vector({
                source:new ol.source.Vector({
                    features: [new ol.Feature({
                        geometry: new ol.geom.Point(ol.proj.transform([parseFloat(lng), parseFloat(lat)], 'EPSG:4326', 'EPSG:3857')),
                    })]
                }),
                style: new ol.style.Style({
                    image: new ol.style.Icon({
                        anchor: [0.5, 0.5],
                        anchorXUnits: "fraction",
                        anchorYUnits: "fraction",
                        src: "/public/images/poi.png"
                    })
                })
            });
            map.addLayer(vectorLayer);
        }
    </script>
@endsection
