@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        @lang( 'labels.ccenter_details_title' ) "{{ $ccenter->cc_name }}"
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.ccenter_details_page.ccenter_name' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $ccenter->cc_name }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.ccenter_details_page.shop_count' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $ccenter->shops_count }}
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.ccenter_details_page.created_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $ccenter->created_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.ccenter_details_page.updated_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $ccenter->updated_at ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">

                    <a href="{{ route( 'edit-center', [ 'id' => $ccenter->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.ccenter_details_page.actions.edit_ccenter' )
                    </a>
                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
                        @lang( 'labels.ccenter_details_page.actions.delete_ccenter' )
                    </button>

                    <div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="col-12 text-center">
                                        <h5 class="modal-title text-center" id="deleteDataModalLabel">
                                            @lang( 'labels.ccenter_deletion.deletion_title', [ 'ccenter_name' => $ccenter->cc_name  ] )
                                        </h5>
                                    </div>
                                </div>

                                <div class="modal-body">
                                    @lang( 'labels.ccenter_deletion.deletion_alert' )
                                </div>
                                <div class="modal-footer">
                                    <div class="col-12 text-center">
                                        <a href="{{ route( 'delete-center', [ 'id' => $ccenter->id ] ) }}" class="btn btn-primary">
                                            @lang( 'labels.ccenter_deletion.deletion_confirm' )
                                        </a>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="{{ route( 'all-shops-centrocommerciale', [ 'ccid' => $ccenter->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.mall_details_page.actions.display_linked_shops' )
                    </a>
                    <a href="{{ route( 'all-centers' ) }}" class="btn btn-primary">
                        @lang( 'labels.back_to_list_label' )
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
