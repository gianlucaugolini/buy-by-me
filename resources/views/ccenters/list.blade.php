@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            @lang( 'labels.ccenters_list_title' )
                        </h4>
                    </div>
                    <div class="">
                        <a href="{{ route( 'export-centers') }}" class="text-center list-group-item list-group-item-action bg-warning">
                            <i class="fa fa-file-download"></i>EXPORT CSV
                        </a>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="list-group">
                            @foreach( $ccenters as $c => $ccenter )
                                <a href="{{ route( 'center-details', [ 'id' => $ccenter->id ] ) }}" class="text-center list-group-item list-group-item-action">
                                    {{ $ccenter->cc_name }}
                                </a>
                            @endforeach
                        </div>

                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $ccenters->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route( 'add-center' ) }}" class="btn btn-primary">
                            @lang( 'labels.ccenter_add_new' )
                        </a>
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
