@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.ccenter_add_page.ccenter_add_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.new_desc_centers')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_ccenter_form" method="post" action="{{ route( 'create-center' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.ccenter_add_page.ccenter_name' )
                                    </label>
                                    {{ Form::input( 'text', 'cc_name', old( 'cc_name' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.ccenter_add_page.submit' )
                                    </button>
                                    <a href="{{ route( 'all-centers' ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
