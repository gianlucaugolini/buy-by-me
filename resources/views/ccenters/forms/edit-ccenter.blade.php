@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.ccenter_edit_page.ccenter_edit_form_title', [ 'cc_name' => $ccenter->cc_name ] )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_centers')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_ccenter_form" method="post" action="{{ route( 'update-center' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.ccenter_edit_page.cc_name' )
                                    </label>
                                    {{ Form::input( 'text', 'cc_name', $ccenter->cc_name, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="mall_id" type="hidden" value="{{ $ccenter->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.ccenter_edit_page.submit' )
                                    </button>
                                    <a href="{{ route( 'center-details', [ 'id' => $ccenter->id ] ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
