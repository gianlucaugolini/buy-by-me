@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        <a href="javascript:window.history.back()">OFFERTE</a> >
                        {{ $title }} "{{ $offer->o_title }}"
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col-12" style="display: none;">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_image' )
                                    </strong>
                                </div>
                                <div class="col-12">
                                    <div>
                                        @if( $offer->o_image )
                                            <img class="img-fluid" src="{{ config( 'cross-storage.remote_path' ) . $offer->o_image }}" alt="" />
                                            <br>
                                            <a href="{{ route( 'offer-upload-image', [ 'id' => $offer->id ] )  }}" class="btn btn-danger btn-sm">Modifica Immagine Offerta</a>
                                        @else
                                            <a href="{{ route( 'offer-upload-image', [ 'id' => $offer->id ] )  }}" class="btn btn-primary btn-sm">Aggiungi Immagine Offerta</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_title' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $offer->o_title }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_cat' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $offer->categoria ? $offer->categoria->name : '-----' }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_shop_ext' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <a href="{{ route( 'shop-details', [ 'id' => $offer->o_shop_ext ] ) }}">
                                    {{ $offer->shop->s_name }}
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.created_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $offer->created_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.updated_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $offer->updated_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.blocked' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <p class="{{ $offer->o_blocked === 0 ? "" : "bg-danger" }}">  {{ $offer->blocked_label }}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.offer_details_page.cancelled' )
                                    </strong>
                                </div>
                                <div class="col">
                                    <p class="{{ $offer->o_cancelled === null ? "" : "bg-danger" }}">  {{ $offer->cancelled_label }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_desc' )
                                    </strong>
                                </div>
                                <div class="col-12">
                                    {{ $offer->o_desc }}
                                </div>
                            </div>

                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_pric' )
                                    </strong>
                                    <div>
                                        {{ $offer->o_pric }}€
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-3">
                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_pric_start' )
                                    </strong>
                                    <div>
                                        {{ $offer->o_pric_start }}€
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-3">
                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_disc' )
                                    </strong>
                                    <div>
                                        {{ $offer->o_disc }}%
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3">
                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_howm' )
                                    </strong>
                                    <div>
                                        {{ $offer->o_howm }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="row">
                                    <div class="col text-center">
                                        <strong>
                                            @lang( 'labels.offer_details_page.offer_views' )
                                        </strong>
                                        <div>
                                            {{ $offer->o_views }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="row">
                                    <div class="col text-center">
                                        <strong>
                                            @lang( 'labels.offer_details_page.offer_likes' )
                                        </strong>
                                        <div>
                                            {{ $offer->o_likes }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="row">
                                    <div class="col text-center">
                                        <strong>
                                            @lang( 'labels.offer_details_page.offer_favs' )
                                        </strong>
                                        <div>
                                            {{ $offer->faved_by_count }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <hr>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_start' )
                                    </strong>
                                    <div>
                                        {{ date( 'd-m-Y H:i:s', $offer->o_start ) }}
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-12 col-md-6">

                            <div class="row">
                                <div class="col text-center">
                                    <strong>
                                        @lang( 'labels.offer_details_page.offer_end' )
                                    </strong>
                                    <div>
                                        {{ date( 'd-m-Y H:i:s', $offer->o_end ) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="card-footer text-center">
                    <a href="{{ route( 'all-offers' ) }}" class="btn btn-primary">
                        @lang( 'labels.back_to_list_label' )
                    </a>

                    @if( $offer->o_blocked === 0 )
                        <a href="{{ route( 'offer-status', [ 'id' => $offer->id, 'status' => 1 ] ) }}" class="btn btn-danger">
                            @lang( 'labels.offer_details_page.actions.block_action' )
                        </a>
                    @else
                        <a href="{{ route( 'offer-status', [ 'id' => $offer->id, 'status' => 0 ] ) }}" class="btn btn-warning">
                            @lang( 'labels.offer_details_page.actions.unblock_action' )
                        </a>
                    @endif

                    @if( $offer->o_cancelled == null )
                        <a href="{{ route( 'offer-cancel', [ 'id' => $offer->id, 'status' => 1 ] ) }}" class="btn btn-danger">
                            @lang( 'labels.offer_details_page.actions.cancel_action' )
                        </a>
                    @else
                        <a href="{{ route( 'offer-cancel', [ 'id' => $offer->id, 'status' => 0 ] ) }}" class="btn btn-warning">
                            @lang( 'labels.offer_details_page.actions.uncancel_action' )
                        </a>
                    @endif

                    <a href="{{ route( 'edit-offer', [ 'id' => $offer->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.offer_details_page.actions.edit_offer' )
                    </a>

                    <a href="{{ route( 'shop-details', [ 'id' => $offer->o_shop_ext ] ) }}" class="btn btn-primary">
                        @lang( 'labels.offer_details_page.actions.offer_owner' )
                    </a>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
