@extends( 'layouts.app' )

@section( 'content' )
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            {{ $title }}
                            @if( \Route::current()->getName() === 'offers-search')
                                -
                            @endif
                            <span class="text-danger">{{$filterLabel}}</span>
                            ({{$offers->count()}})
                        </h4>
                    </div>
                    <div class="">
                        <a href="{{ route( 'export-offers') }}" class="text-center list-group-item list-group-item-action bg-warning">
                            <i class="fa fa-file-download"></i>EXPORT CSV
                        </a>
                    </div>
                    <div class="card-body">
                        @if ( session( 'status' ) )
                            <div class="alert alert-success" role="alert">
                                {{ session( 'status' ) }}
                            </div>
                        @endif
                            <div class="col-12 col-md-12">
                                <div class="row">
                                    <div class="text-left col-lm-4">
                                        <form class="form-inline" method="post">
                                            @csrf
                                            <div class="form-group">
                                                <input type="text" class="form-control mr-3" name="searchOffer" id="searchOffer" placeholder="Cerca offerta"
                                                @if( !empty($search))
                                                value="{{$search}}"
                                                @endif
                                                >
                                            </div>
                                            <button type="submit" class="btn btn-primary mb-2">Cerca</button>
                                        </form>
                                    </div>
                                    <div class="col text-center">
                                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapsableInfo" aria-expanded="true" aria-controls="collapsableInfo">
                                            Dettagli
                                        </button>
                                    </div>
                                    <div class="text-right col">
                                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                            <div class="btn-group" role="group"
                                                 @if( !empty($filterLabel))
                                                    class="bg-warning"
                                                @endif >
                                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Filtra
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1"                                                 >
                                                    <a class="dropdown-item" href="{{ route('all-offers') }}">Tutte</a>
                                                    <a class="dropdown-item" class="{{ !empty($filter) ? ((strpos($filter, 'running') !== false) ? "bg-warning" : "") : "" }}" href="{{ route('all-offers', ['filter' => 'running']) }}">In corso</a>
                                                    <a class="dropdown-item" class="{{ !empty($filter) ? ((strpos($filter, 'ended') !== false) ? "bg-warning" : "") : "" }}" href="{{ route('all-offers', ['filter' => 'ended']) }}">Terminate</a>
                                                    <a class="dropdown-item" class="{{ !empty($filter) ? ((strpos($filter, 'blocked') !== false) ? "bg-warning" : "") : "" }}" href="{{ route('all-offers', ['filter' => 'blocked']) }}">Bloccate</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <div class="list-group text-center">
                            @foreach( $offers as $o => $offer )
                                <li class="list-group-item row">
                                    <div>
                                        <a href="{{ route( 'offers-details', [ 'id' => $offer->id ] ) }}" class="text-center list-group-item list-group-item-action
                                         @if( $offer->o_blocked === 1 )
                                            bg-danger text-light
                                         @elseif (now()->timestamp < $offer->o_end)
                                            bg-success
                                        @else
                                            bg-secondary text-light
                                        @endif
                                      ">
                                            {{ $offer->o_title }} - {{ $offer->shop->s_name }} (cat: {{$offer->categoria->name}})
                                            @if( $offer->o_blocked === 1 )
                                                <i class="fa fa-ban" aria-label="Offerta bloccata" data-toggle="tooltip"  data-placement="right" title="Offerta bloccata" style="font-size:20px;color:red;"></i>
                                            @endif
                                            @if( $offer->o_cancelled != null )
                                                <i class="fa fa-window-close" aria-label="Offerta annullata" data-toggle="tooltip"  data-placement="right" title="Offerta annullata" style="font-size:20px;color:red;"></i>
                                            @endif
                                        </a>
                                       <hr>
                                        <div id="collapsableInfo" class="collapse show">
                                            <div class="row">
                                                <div class="col-12 col-md-3">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_pric' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ $offer->o_pric }}€
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-12 col-md-3">

                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_disc' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ $offer->o_disc }}%
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_howm' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ $offer->o_howm }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_views' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ $offer->o_views }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <hr>

                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_start' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ date( 'd-m-Y H:i:s', $offer->o_start ) }}
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-12 col-md-6">

                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_end' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            {{ date( 'd-m-Y H:i:s', $offer->o_end ) }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-12 col-md-6">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_shop_ext' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            <a href="{{ route( 'shop-details', [ 'id' => $offer->o_shop_ext ] ) }}">
                                                                {{ $offer->shop->s_name }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.blocked' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            <p class="{{ $offer->o_blocked === 0 ? "" : "bg-danger" }}">  {{ $offer->blocked_label }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_views' )
                                                            </strong>
                                                        </div>
                                                      <div class="col">
                                                            <p> Views: {{ $offer->o_views }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <div class="row">
                                                        <div class="col">
                                                            <strong>
                                                                @lang( 'labels.offer_details_page.offer_likes' )
                                                            </strong>
                                                        </div>
                                                        <div class="col">
                                                            <p> Likes: {{ $offer->o_likes }}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </li>
                            @endforeach
                        </div>

                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $offers->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
