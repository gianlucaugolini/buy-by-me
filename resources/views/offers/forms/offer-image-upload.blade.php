@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.offer_add_image_page.offer_add_image_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_offer_image')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_shop_image" method="post" action="{{ route( 'offer-save-image' ) }}" enctype="multipart/form-data">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.offer_add_image_page.offer_image' )
                                    </label>
                                    {{ Form::file( 'offer_image', old( 'offer_image' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="offer_id" type="hidden" value="{{ $offer->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.offer_add_image_page.submit' )
                                    </button>
                                    <a href="{{ route( 'offers-details', [ 'id' => $offer->id ] ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
