@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.offer_edit_page.offers_edit_form_title', [ 'offer_title' => $offer->o_title ] )
                        </h5>
                        <p class="card-text">@lang( 'labels.edit_desc_offers')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="edit_offer_form" method="post" action="{{ route( 'update-offer' ) }}" class="offer-form">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <label for="first_name">
                                        @lang( 'labels.offer_edit_page.offer_title' )
                                    </label>
                                    {{ Form::input( 'text', 'o_title', $offer->o_title, [ 'class' => 'form-control' ] ) }}
                                </div>
                                <div class="form-group col-12">
                                    <label for="last_name">
                                        @lang( 'labels.offer_edit_page.offer_desc' )
                                    </label>
                                    {{ Form::textarea( 'o_desc', $offer->o_desc, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12 col-md-3">
                                    <label for="first_name">
                                        @lang( 'labels.offer_edit_page.offer_pric_start' )
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">&euro;<small>( xx.xx )</small></span>
                                        </div>
                                        {{ Form::input( 'text', 'o_pric_start', $offer->o_pric_start, [ 'class' => 'form-control' ] ) }}
                                    </div>

                                </div>
                                <div class="form-group col-12 col-md-3">
                                    <label for="first_name">
                                        @lang( 'labels.offer_edit_page.offer_pric' )
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">&euro;<small>( xx.xx )</small></span>
                                        </div>
                                        {{ Form::input( 'text', 'o_pric', $offer->o_pric, [ 'class' => 'form-control' ] ) }}
                                    </div>

                                </div>
                                <div class="form-group col-12 col-md-3">
                                    <label for="first_name">
                                        @lang( 'labels.offer_edit_page.offer_disc' )
                                    </label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                        {{ Form::input( 'text', 'o_disc', $offer->o_disc, [ 'class' => 'form-control' ] ) }}
                                    </div>
                                </div>
                                <div class="form-group col-12 col-md-3">
                                    <label for="first_name">
                                        @lang( 'labels.offer_edit_page.offer_howm' )
                                    </label>
                                    {{ Form::input( 'text', 'o_howm', $offer->o_howm, [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12 col-md-6">
                                    <label for="o_start">
                                        @lang( 'labels.offer_edit_page.offer_start' )
                                    </label>
                                    <div class="input-group date" id="start" data-target-input="nearest">
                                        {{ Form::input( 'text', 'o_start', \Carbon\Carbon::createFromTimestamp( $offer->o_start ), [ 'class' => 'form-control datetimepicker-input', 'data-target' => '#start' ] ) }}
                                        <div class="input-group-append" data-target="#start" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-12 col-md-6">
                                    <label for="o_end">
                                        @lang( 'labels.offer_edit_page.offer_end' )
                                    </label>
                                    <div class="input-group date" id="end" data-target-input="nearest">
                                        {{ Form::input( 'text', 'o_end', \Carbon\Carbon::createFromTimestamp( $offer->o_end ), [ 'class' => 'form-control datetimepicker-input', 'data-target' => '#end' ] ) }}
                                        <div class="input-group-append" data-target="#end" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-12">
                                    <div>
                                        <label for="s_cat">
                                            @lang( 'labels.offer_edit_page.offer_cat' )
                                        </label>
                                    </div>
                                    @foreach( $categories as $c => $category )
                                        <div class="custom-control custom-switch">
                                            {{ Form::radio( 'o_cat', $category->id, $category->id === $offer->o_cat, [ 'class' => 'custom-control-input', 'id' => 'oc-' . $category->id ] ) }}
                                            <label class="custom-control-label" for="oc-{{ $category->id }}">
                                                {{ $category->name }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    <input name="offer_id" type="hidden" value="{{ $offer->id }}">
                                    <button type="submit" class="btn btn-primary">
                                        @lang( 'labels.offer_edit_page.submit' )
                                    </button>
                                    <button onclick="location.href='{{ url()->previous() }}'" class="btn btn-warning">
                                        @lang( 'labels.annulla' )
                                    </button>
                                    <a href="{{ route( 'offers-details', [ 'id' => $offer->id ] ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@section( 'scripts' )
    <script type="text/javascript">
        $(function () {

            $('#start').datetimepicker({
                format: 'Y/MM/DD HH:mm:ss',
                useCurrent: false,
                showTodayButton: true
            });
            $('#end').datetimepicker({
                format: 'Y/MM/DD HH:mm:ss',
                useCurrent: false,
                showTodayButton: true
            });

            //constrain
            $("#start").on("dp.change", function (e) {
                $('#end').data("DateTimePicker").minDate(e.date); //same day available
            });

            $("#end").on("dp.change", function (e) {
                $('#start').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endsection
