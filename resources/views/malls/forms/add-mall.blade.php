@extends('layouts.app')

@section('content')
    <div class="container">
        @include( 'ui.form-validation-messages' )
        @include( 'ui.messages' )

        <div class="row mb-2">
            <div class="col-sm">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">
                            @lang( 'labels.mall_add_page.mall_add_form_title' )
                        </h5>
                        <p class="card-text">@lang( 'labels.new_desc_malls')</p>
                    </div>
                </div>
            </div>
        </div>
        <form name="add_mall_form" method="post" action="{{ route( 'create-mall' ) }}">
            @csrf
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col">
                                    <label for="first_name">
                                        @lang( 'labels.mall_add_page.mall_name' )
                                    </label>
                                    {{ Form::input( 'text', 'cc_name', old( 'cc_name' ), [ 'class' => 'form-control' ] ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                @if( count( $supervisors_select ) > 0 )
                                    <div class="form-group col-12">
                                        <label for="s_address">
                                            @lang( 'labels.mall_add_page.supervisor' )
                                        </label>
                                        {{ Form::select( 'supervisor_id', $supervisors_select, old( 'supervisor_id' ), [ 'class' => 'form-control', 'placeholder' => 'Selezionare un Supervisore tra quelli disponibili...' ] ) }}
                                    </div>
                                    <div>
                                        <a href="{{ route( 'add-supervisor' ) }}" class="btn btn-primary">
                                            @lang( 'labels.user_add.add_new_supervisor' )
                                        </a>
                                    </div>
                                @else
                                <div class="float-right">
                                    @lang( 'labels.mall_add_page.mall_add_no_supervisor' )
                                    <a href="{{ route( 'add-supervisor' ) }}" class="btn btn-primary">
                                        @lang( 'labels.user_add.add_new_supervisor' )
                                    </a>
                                </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-sm">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="col text-center">
                                    @if( count( $supervisors_select ) > 0 )
                                    <button type="submit" class="btn btn-primary btn-disabled">
                                        @lang( 'labels.mall_add_page.submit' )
                                    </button>
                                    @endif
                                    <a href="{{ route( 'all-malls' ) }}" class="btn btn-primary">
                                        @lang( 'labels.back_label' )
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection
