@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4 class="text-center m-0">
                            {{ $title }}
                        </h4>
                    </div>
                    <div class="">
                        <a href="{{ route( 'export-malls') }}" class="text-center list-group-item list-group-item-action bg-warning">
                            <i class="fa fa-file-download"></i>EXPORT CSV
                        </a>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="list-group">
                            @foreach( $malls as $m => $mall )
                                <a href="{{ route( 'mall-details', [ 'id' => $mall->id ] ) }}" class="text-center list-group-item list-group-item-action" data-toggle="tooltip" data-placement="right" title="click per visualizzare i dettagli della Catena Commerciale">
                                    {{ $mall->cc_name }}
                                </a>
                                    @if( count( $mall->supervisori ) > 0 )
                                        @foreach( $mall->supervisori as $s => $supervisore )

                                            <a data-toggle="tooltip" data-placement="right" title="click per visualizzare i dettagli del Supervisore" href="{{ route( 'user-details', [ 'id' => $supervisore->user_id ] ) }}" class="btn btn-primary btn-sm ml-2">  <i class="fa fa-user"></i> Supervisore: {{ $supervisore->ccs_name . ' ' . $supervisore->ccs_last_name }}</a>
                                        <br>
                                        @endforeach
                                    @else
                                        <span class="btn btn-warning btn-sm ml-2">Nessun supervisore.</span><br>
                                    @endif
                            @endforeach
                        </div>

                        <div class="row mt-2">
                            <div class="col text-center">
                                {{ $malls->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div>

                    <div class="card-footer text-center">
                        <a href="{{ route( 'add-mall' ) }}" class="btn btn-primary">
                            @lang( 'labels.mall_add_new' )
                        </a>
                        <a href="{{ route( 'home' ) }}" class="btn btn-primary">
                            @lang( 'labels.back_to_home_label' )
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
