@extends('layouts.app')

@section('content')
<div class="container">
    @include( 'ui.messages' )
    <div class="row justify-content-center">
        <div class="col-md-12 col-lg-10">
            <div class="card">
                <div class="card-header">
                    <h4 class="text-center m-0">
                        @lang( 'labels.mall_details_title' ) "{{ $catena->cc_name }}"
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.mall_details_page.mall_name' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ $catena->cc_name }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.mall_details_page.shop_count' )
                                    </strong>
                                </div>
                                <div class="col">
                                    @if( count( $catena->supervisori ) > 0 )
                                        {{ count($catena->shops)}} {{ count($catena->shops) > 1 ? "negozi" : "negozio"}}
                                    @else
                                        <li class="text-center list-group-item list-group-item-action">
                                            Nessun shop associato a questa catena commerciale
                                        </li>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.mall_details_page.created_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $catena->created_at ) }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <strong>
                                        @lang( 'labels.mall_details_page.updated_at' )
                                    </strong>
                                </div>
                                <div class="col">
                                    {{ date( 'd-m-Y H:i:s', $catena->updated_at ) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <strong>
                                        @lang( 'labels.mall_details_page.supervisors' )
                                    </strong>
                                </div>
                                @if( count( $catena->supervisori ) > 0 )
                                    @foreach( $catena->supervisori as $s => $supervisore )
                                        <div class="col-12 col-md-6">
                                            {{ $supervisore->ccs_name . ' ' . $supervisore->ccs_last_name }} <a href="{{ route( 'remove-supervisor', [ 'ccid' => $catena->id, 'sid' => $supervisore->user_id ] ) }}" class="btn btn-danger btn-sm ml-4">Rimuovi Supervisore</a> <a href="{{ route( 'user-details', [ 'id' => $supervisore->user_id ] ) }}" class="btn btn-primary btn-sm ml-2">Vedi Supervisore</a>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="col-12">
                                        @lang( 'labels.mall_details_page.no_supervisors_yet' )
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer text-center">
                    <a href="{{ route( 'edit-mall', [ 'id' => $catena->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.mall_details_page.actions.edit_mall' )
                    </a>
                    <button type="button" class="btn btn-danger mb-1" data-toggle="modal" data-target="#deleteDataModal">
                        @lang( 'labels.mall_details_page.actions.delete_mall' )
                    </button>

                    <div class="modal fade" id="deleteDataModal" tabindex="-1" role="dialog" aria-labelledby="deleteDataModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="col-12 text-center">
                                        <h5 class="modal-title text-center" id="deleteDataModalLabel">
                                            @lang( 'labels.mall_deletion.deletion_title', [ 'mall_name' => $catena->cc_name  ] )
                                        </h5>
                                    </div>
                                </div>

                                <div class="modal-body">
                                    @lang( 'labels.mall_deletion.deletion_alert' )
                                </div>
                                <div class="modal-footer">
                                    <div class="col-12 text-center">
                                        <a href="{{ route( 'delete-mall', [ 'id' => $catena->id ] ) }}" class="btn btn-primary">
                                            @lang( 'labels.mall_deletion.deletion_confirm' )
                                        </a>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="{{ route( 'all-shops-catenacommerciale', [ 'ccid' => $catena->id ] ) }}" class="btn btn-primary">
                        @lang( 'labels.mall_details_page.actions.display_linked_shops' )
                    </a>
                    <a href="{{ route( 'all-malls' ) }}" class="btn btn-primary">
                        @lang( 'labels.back_to_list_label' )
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection
