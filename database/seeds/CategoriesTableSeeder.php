<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Category::truncate();
        $categories = [
            [ 'name' => 'Vestiario' ],
            [ 'name' => 'Alimentari' ],
            [ 'name' => 'Elettronica' ],
            [ 'name' => 'Viaggi' ],
            [ 'name' => 'Gioielli' ],
            [ 'name' => 'Casa' ],
            [ 'name' => 'Bellezza' ],
            [ 'name' => 'Giochi' ],
            [ 'name' => 'Divertimento' ],
            [ 'name' => 'Ristoranti' ],
            [ 'name' => 'Farmacie' ],
            [ 'name' => 'Salute' ],
            [ 'name' => 'Professionisti' ],
            [ 'name' => 'Tecnici' ],
            [ 'name' => 'Varie' ],
        ];

        Category::insert( $categories );

    }
}
