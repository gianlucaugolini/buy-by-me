<?php

use App\Models\Commerciant;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommerciantsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        /* Commerciant Users */

        $commerciants = [];
        $commerciants_details = [];


        $faker = Faker\Factory::create('it_IT');

        for( $i = 0; $i < 10; $i++ ) {
            $email = $faker->email;

            $commerciants[] = [
                'fullname'  =>  $faker->name,
                'email'     =>  $email,
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>  User::COMMERCIANT,

            ];

        }

        $commerciants[] = [
            'fullname'  =>  "Andrea Leganza COM",
            'email'     =>  "neogene@com.it",
            'password'  =>  bcrypt( 'test1234' ),
            'type_id'   =>  User::COMMERCIANT,
        ];

        foreach ( $commerciants as $c => $commerciant ) {
            $u = User::create( $commerciant );

            $offers = rand(0, 10);
            $userNameArr = explode(" ", $u->fullname);

            $commerciants_details = [
                'user_id'       => $u->id,
                'c_name'        => $userNameArr[0], // $faker->firstName,
                'c_last_name'   =>  $userNameArr[1], // $faker->lastName,
                'c_offers_bought' => $offers,
                'c_offers_used' => rand(0,$offers)
            ];

            Commerciant::create( $commerciants_details );
        }

    }
}
