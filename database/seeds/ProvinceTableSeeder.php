<?php

use App\Models\Provincia;
use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Provincia::truncate();

        $province = [
            ['prov_code'  => 'TO',  'prov' => 'Torino' ],
            ['prov_code'  => 'VC',  'prov' => 'Vercelli' ],
            ['prov_code'  => 'NO',  'prov' => 'Novara' ],
            ['prov_code'  => 'CN',  'prov' => 'Cuneo' ],
            ['prov_code'  => 'AT',  'prov' => 'Asti' ],
            ['prov_code'  => 'AL',  'prov' => 'Alessandria' ],
            ['prov_code'  => 'BI',  'prov' => 'Biella' ],
            ['prov_code'  => 'VB',  'prov' => 'Verbano-Cusio-Ossola' ],
            ['prov_code'  => 'AO',  'prov' => 'Aosta' ],
            ['prov_code'  => 'VA',  'prov' => 'Varese' ],
            ['prov_code'  => 'CO',  'prov' => 'Como' ],
            ['prov_code'  => 'SO',  'prov' => 'Sondrio' ],
            ['prov_code'  => 'MI',  'prov' => 'Milano' ],
            ['prov_code'  => 'BG',  'prov' => 'Bergamo' ],
            ['prov_code'  => 'BS',  'prov' => 'Brescia' ],
            ['prov_code'  => 'PV',  'prov' => 'Pavia' ],
            ['prov_code'  => 'CR',  'prov' => 'Cremona' ],
            ['prov_code'  => 'MN',  'prov' => 'Mantova' ],
            ['prov_code'  => 'LC',  'prov' => 'Lecco' ],
            ['prov_code'  => 'LO',  'prov' => 'Lodi' ],
            ['prov_code'  => 'BZ',  'prov' => 'Bolzano' ],
            ['prov_code'  => 'TN',  'prov' => 'Trento' ],
            ['prov_code'  => 'VR',  'prov' => 'Verona' ],
            ['prov_code'  => 'VI',  'prov' => 'Vicenza' ],
            ['prov_code'  => 'BL',  'prov' => 'Belluno' ],
            ['prov_code'  => 'TV',  'prov' => 'Treviso' ],
            ['prov_code'  => 'VE',  'prov' => 'Venezia' ],
            ['prov_code'  => 'PD',  'prov' => 'Padova' ],
            ['prov_code'  => 'RO',  'prov' => 'Rovigo' ],
            ['prov_code'  => 'UD',  'prov' => 'Udine' ],
            ['prov_code'  => 'GO',  'prov' => 'Gorizia' ],
            ['prov_code'  => 'TS',  'prov' => 'Trieste' ],
            ['prov_code'  => 'PN',  'prov' => 'Pordenone' ],
            ['prov_code'  => 'IM',  'prov' => 'Imperia' ],
            ['prov_code'  => 'SV',  'prov' => 'Savona' ],
            ['prov_code'  => 'GE',  'prov' => 'Genova' ],
            ['prov_code'  => 'SP',  'prov' => 'La Spezia' ],
            ['prov_code'  => 'PC',  'prov' => 'Piacenza' ],
            ['prov_code'  => 'PR',  'prov' => 'Parma' ],
            ['prov_code'  => 'RE',  'prov' => 'Reggio nell\'Emilia' ],
            ['prov_code'  => 'MO',  'prov' => 'Modena' ],
            ['prov_code'  => 'BO',  'prov' => 'Bologna' ],
            ['prov_code'  => 'FE',  'prov' => 'Ferrara' ],
            ['prov_code'  => 'RA',  'prov' => 'Ravenna' ],
            ['prov_code'  => 'FO',  'prov' => 'Forli\'-Cesena' ],
            ['prov_code'  => 'RN',  'prov' => 'Rimini' ],
            ['prov_code'  => 'MS',  'prov' => 'Massa-Carrara' ],
            ['prov_code'  => 'LU',  'prov' => 'Lucca' ],
            ['prov_code'  => 'PT',  'prov' => 'Pistoia' ],
            ['prov_code'  => 'FI',  'prov' => 'Firenze' ],
            ['prov_code'  => 'LI',  'prov' => 'Livorno' ],
            ['prov_code'  => 'PI',  'prov' => 'Pisa' ],
            ['prov_code'  => 'AR',  'prov' => 'Arezzo' ],
            ['prov_code'  => 'SI',  'prov' => 'Siena' ],
            ['prov_code'  => 'GR',  'prov' => 'Grosseto' ],
            ['prov_code'  => 'PO',  'prov' => 'Prato' ],
            ['prov_code'  => 'PG',  'prov' => 'Perugia' ],
            ['prov_code'  => 'TR',  'prov' => 'Terni' ],
            ['prov_code'  => 'PS',  'prov' => 'Pesaro e Urbino' ],
            ['prov_code'  => 'AN',  'prov' => 'Ancona' ],
            ['prov_code'  => 'MC',  'prov' => 'Macerata' ],
            ['prov_code'  => 'AP',  'prov' => 'Ascoli Piceno' ],
            ['prov_code'  => 'VT',  'prov' => 'Viterbo' ],
            ['prov_code'  => 'RI',  'prov' => 'Rieti' ],
            ['prov_code'  => 'RM',  'prov' => 'Roma' ],
            ['prov_code'  => 'LT',  'prov' => 'Latina' ],
            ['prov_code'  => 'FR',  'prov' => 'Frosinone' ],
            ['prov_code'  => 'AQ',  'prov' => 'L\'Aquila' ],
            ['prov_code'  => 'TE',  'prov' => 'Teramo' ],
            ['prov_code'  => 'PE',  'prov' => 'Pescara' ],
            ['prov_code'  => 'CH',  'prov' => 'Chieti' ],
            ['prov_code'  => 'CB',  'prov' => 'Campobasso' ],
            ['prov_code'  => 'IS',  'prov' => 'Isernia' ],
            ['prov_code'  => 'CE',  'prov' => 'Caserta' ],
            ['prov_code'  => 'BN',  'prov' => 'Benevento' ],
            ['prov_code'  => 'NA',  'prov' => 'Napoli' ],
            ['prov_code'  => 'AV',  'prov' => 'Avellino' ],
            ['prov_code'  => 'SA',  'prov' => 'Salerno' ],
            ['prov_code'  => 'FG',  'prov' => 'Foggia' ],
            ['prov_code'  => 'BA',  'prov' => 'Bari' ],
            ['prov_code'  => 'TA',  'prov' => 'Taranto' ],
            ['prov_code'  => 'BR',  'prov' => 'Brindisi' ],
            ['prov_code'  => 'LE',  'prov' => 'Lecce' ],
            ['prov_code'  => 'PZ',  'prov' => 'Potenza' ],
            ['prov_code'  => 'MT',  'prov' => 'Matera' ],
            ['prov_code'  => 'CS',  'prov' => 'Cosenza' ],
            ['prov_code'  => 'CZ',  'prov' => 'Catanzaro' ],
            ['prov_code'  => 'RC',  'prov' => 'Reggio di Calabria' ],
            ['prov_code'  => 'KR',  'prov' => 'Crotone' ],
            ['prov_code'  => 'VV',  'prov' => 'Vibo Valentia' ],
            ['prov_code'  => 'TP',  'prov' => 'Trapani' ],
            ['prov_code'  => 'PA',  'prov' => 'Palermo' ],
            ['prov_code'  => 'ME',  'prov' => 'Messina' ],
            ['prov_code'  => 'AG',  'prov' => 'Agrigento' ],
            ['prov_code'  => 'CL',  'prov' => 'Caltanissetta' ],
            ['prov_code'  => 'EN',  'prov' => 'Enna' ],
            ['prov_code'  => 'CT',  'prov' => 'Catania' ],
            ['prov_code'  => 'RG',  'prov' => 'Ragusa' ],
            ['prov_code'  => 'SR',  'prov' => 'Siracusa' ],
            ['prov_code'  => 'SS',  'prov' => 'Sassari' ],
            ['prov_code'  => 'NU',  'prov' => 'Nuoro' ],
            ['prov_code'  => 'CA',  'prov' => 'Cagliari' ],
            ['prov_code'  => 'OR',  'prov' => 'Oristano' ],
        ];


        usort($province,function($first,$second){
            return $first["prov"] > $second["prov"];
        });

        Provincia::insert( $province );

    }
}
