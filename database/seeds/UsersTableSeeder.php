<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        User::truncate();


        /* Admin Users */
        $users = [
            [
                'fullname'  =>  'Gianluca Ugolini',
                'email'     =>  'gianluca.ugolini@gmail.com',
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>   User::ADMINISTRATOR,
                'is_superadmin' => 1
            ],
            [
                'fullname'  =>  'Andrea Leganza ADMIN',
                'email'     =>  'neogene@admin.it',
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>  User::ADMINISTRATOR,
                'is_superadmin' => 1
            ],
            [
                'fullname'  =>  'Marco Bertini',
                'email'     =>  'marco.bertini@buybyme.net',
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>  User::ADMINISTRATOR,
                'is_superadmin' => 1
            ],
        ];

        foreach ( $users as $u => $user ) {
            User::create( $user );
        }

    }
}
