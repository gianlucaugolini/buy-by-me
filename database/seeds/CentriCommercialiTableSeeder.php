<?php

use App\Models\CentroCommerciale;
use Illuminate\Database\Seeder;

class CentriCommercialiTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {


        CentroCommerciale::truncate();

        $centri = [];

        $faker = Faker\Factory::create('it_IT');

        for( $i = 0; $i < 10; $i++ ) {

            $company = $faker->company;

            $centri[] = [
                'cc_name'    =>  $company
            ];
        }

        foreach ( $centri as $c => $centro ) {

            CentroCommerciale::create( $centro );
        }



    }
}
