<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call( ProvinceTableSeeder::class );
        $this->call( UsersTableSeeder::class );
        $this->call( CustomersTableSeeder::class );
        $this->call( CommerciantsTableSeeder::class );
        $this->call( CateneCommercialiTableSeeder::class );
        $this->call( CentriCommercialiTableSeeder::class );
        $this->call( CategoriesTableSeeder::class );
        $this->call( ShopsTableSeeder::class );
        $this->call( OffersTableSeeder::class );
    }
}
