<?php

use App\Models\Customer;
use App\Models\User;
use App\Models\Provincia;
use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Customer::truncate();

        /* Customers Users */

        $customers = [];
        $customers_details = [];


        $faker = Faker\Factory::create('it_IT');

        for( $i = 0; $i < 10; $i++ ) {
            $email = $faker->email;

            $customers[] = [
                'fullname'  =>  $faker->name,
                'email'     =>  $email,
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>  User::CUSTOMER,

            ];

        }

        $customers[] = [
            'fullname'  =>  "Andrea Leganza",
            'email'     =>  "neogene@tin.it",
            'password'  =>  bcrypt( 'test1234' ),
            'type_id'   =>  User::CUSTOMER,

        ];

        $provinces = Provincia::get();

        foreach ( $customers as $c => $customer ) {
            $u = User::create( $customer );

            $province = $provinces[rand(0,count($provinces)-1)];

            $randomCategories = array();
            for ($i=0; $i < 15; $i++) {
                // get a random digit, but always a new one, to avoid duplicates
                $randomCategories []= $faker->numberBetween($min = 0, $max = 1);
            }

            $randomDiscounts = array();
            for ($i=0; $i < 9; $i++) {
                // get a random digit, but always a new one, to avoid duplicates
                $randomDiscounts []= $faker->numberBetween($min = 0, $max = 1);
            }

            $customer_user_details = [
                'user_id'       => $u->id,
                'u_name'        => $faker->firstName,
                'u_last_name'   => $faker->lastName,
                'u_prov'        => $province->prov,
                "u_prov_code"   => $province->prov_code,
                "u_notification_ios_on" => 1,
                "u_notification_android_on" => 1,
                "u_notification_only_filters" => 0,
                "u_notification_categories" => implode(";",$randomCategories).";",
                "u_notification_discounts" => implode(";",$randomDiscounts).";",
                "u_confirmed"   => $faker->numberBetween($min = 0, $max = 1)
            ];

            //test fcm
            if ($customer["email"] == "neogene@tin.it"){
                $customer_user_details["u_notification_ios_token"]     = "eYw-XF4frVE:APA91bG40zfqcokDfrNMfVfDCTen5GBk8FCW5irHO2u_c3T_8rcuMut2JbbPEEkQ22UKNLDleClEVspWd7jzIsAXAccMvkpljYzz1GtY-BeLdO4_c_8hbGNU_AmhLrCIQm6nlG4RTBKN";
                $customer_user_details["u_notification_android_token"] = "dFK83apNFM0:APA91bGIP9LRPv5ccytb-AIeLsavuU6UzjihlTqqPmroTC5PBWoLmEUiwn_24U-RDFxi6ot0ka5fR7iQaVSQJ4EO_Nbi9coaFQ48sI12GVtXojGbr_WZSn-cvJzK5PiU1_7uf7GNE-dX";
            }

            Customer::create( $customer_user_details );
        }

    }
}
