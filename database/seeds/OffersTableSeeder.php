<?php

use App\Models\Customer;
use App\Models\Offer;
use Illuminate\Database\Seeder;
use Faker\Provider\DateTime as DateTimeProvider;

class OffersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Offer::truncate();

        $offers = [];


        $faker = Faker\Factory::create('it_IT');

        for ($i = 1; $i < 11; $i++) {

            $company = $faker->company;
            $company2 = $faker->company;

            $offers[] = [
                'o_title' => $faker->text(10),
                'o_desc' => $faker->text,
                'o_disc' => $faker->numberBetween(5,90),
                'o_howm' => $faker->numberBetween(0,5000),
                'o_cat' => $faker->numberBetween(1,15),
                'o_shop_ext' => $i,
                'o_pric'    => $faker->numberBetween(0,500),
                'o_uid' => $i,
                'o_start' => time(),
                'o_end' => time()+$faker->numberBetween(86400,2629743) //1 day - 1 month
            ];
        }

        foreach ( $offers as $o => $offer ) {
            $offer_created = Offer::create( $offer );

            $all_customers = Customer::get();

            foreach ( $all_customers as $ac => $customer ) {
                $is_fav = (bool)random_int(0, 1);
                if ( $is_fav ) {
                    $customer->favs()->attach( $offer_created->id );
                }
            }
        }

    }
}
