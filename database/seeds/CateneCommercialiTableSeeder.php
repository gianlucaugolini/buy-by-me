<?php

use App\Models\Catena;
use App\Models\Supervisor;
use App\Models\User;
use Illuminate\Database\Seeder;

class CateneCommercialiTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Catena::truncate();
        Supervisor::truncate();

        $catene = [];


        $faker = Faker\Factory::create('it_IT');

        for( $i = 0; $i < 10; $i++ ) {

            $company = $faker->company;

            $catene[] = [
                'cc_name'    =>  $company
            ];
        }

        foreach ( $catene as $c => $catena ) {

            $email = $faker->email;
            $firstName = $faker->firstName;
            $lastName = $faker->lastName;
            $fullname = $firstName . ' ' . $lastName;

            $user = [
                'fullname'  =>  $fullname,
                'email'     =>  $email,
                'password'  =>  bcrypt( 'test1234' ),
                'type_id'   =>  User::SUPERVISOR,

            ];

            $u = User::create( $user );

            $created = Catena::create( $catena );

            $supervisor = [
                'user_id'       =>  $u->id,
                'ccs_name'      =>  $firstName,
                'ccs_last_name' =>  $lastName,
                'cc_ext'        =>  $created->id
            ];

            Supervisor::create( $supervisor );
        }



    }
}
