<?php

use App\Models\Catena;
use App\Models\CentroCommerciale;
use App\Models\Commerciant;
use App\Models\Shop;
use App\Models\User;
use Illuminate\Database\Seeder;
use App\Models\Provincia;

class ShopsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        Shop::truncate();

        $users = Commerciant::get();

        $faker = Faker\Factory::create('it_IT');

        $provinces = Provincia::get();

        foreach( $users as $u => $user ) {

            $catena = Catena::get()->random( 1 )->first()->id;
            $centro_commerciale = CentroCommerciale::get()->random( 1 )->first()->id;
            $company = $faker->company;

            $province = $provinces[rand(0,count($provinces)-1)];

            $shop = [
                's_commerciant_ext'             =>  $user->id,
                's_name'                        =>  $company,
                's_desc'                        =>  $faker->text,
                's_cat'                         =>  $faker->numberBetween( 1, 15 ),
                's_catena_commerciale_ext'      =>  $catena,
                's_centro_commerciale_ext'      =>  $centro_commerciale,
                's_centro_commerciale_piano'    =>  1,
                's_prd'                         =>  $faker->text(100),
                's_lat'                         =>  $faker->latitude( 41.9109-0.05,41.9109+0.05 ),
                's_lon'                         =>  $faker->longitude( 12.4818-0.05,12.4818+0.05 ),
                's_address'                     =>  $faker->streetAddress,
                's_cap'                         =>  $faker->postCode,
                's_city'                        =>  $faker->city,
                's_prov'                        => $province->prov,
                's_prov_code'                   => $province->prov_code
            ];

            Shop::create( $shop );

        }

    }
}
