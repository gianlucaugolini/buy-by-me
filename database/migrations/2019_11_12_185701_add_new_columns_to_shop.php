<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnsToShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('shop', function ( Blueprint $table ) {
            $table->string( 's_web', 64 )->default( '' );
            $table->string( 's_phone',24 );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('shop', function ( Blueprint $table ) {
            $table->dropColumn('s_web' );
            $table->dropColumn('s_phone' );
        });
    }
}
