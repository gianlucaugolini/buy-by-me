<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('offer', function ( Blueprint $table ) {
            $table->increments('id');
            $table->tinyInteger( 'o_blocked' )->unsigned()->default( 0 );
            $table->string( 'o_title', 255 );
            $table->string( 'o_image_name', 191 )->nullable( true )->default( null );
            $table->string( 'o_image', 191 )->nullable( true )->default( null );
            $table->string( 'o_image_real_path', 191 )->nullable( true )->default( null );
            $table->string( 'o_image_path', 191 )->nullable( true )->default( null );
            $table->text( 'o_desc' )->nullable( true );
            $table->float( 'o_pric' )->default( 0 );
            $table->smallInteger( 'o_disc' )->unsigned();
            $table->smallInteger( 'o_howm' )->unsigned();
            $table->timestamp( 'o_start' )->nullable( true );
            $table->timestamp( 'o_end' )->nullable( true );
            $table->tinyInteger( 'o_cat' );
            $table->integer( 'o_shop_ext' );
            $table->string( 'o_uid', 64 )->nullable( true );
            $table->integer( 'o_views' )->unsigned()->default( 0 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'offer' );
    }
}
