<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCateneCommercialiSupervisorsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('supervisor', function (Blueprint $table) {
            $table->increments('id');
            $table->integer( 'user_id' )->unsigned();
            $table->string( 'ccs_name', 128 )->nullable( true );
            $table->string( 'ccs_last_name', 128 )->nullable( true );
            $table->integer('cc_ext' )->unsigned()->nullable()->default( null );
            $table->tinyInteger('ccs_blocked' )->default( 0 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('supervisor');
    }
}
