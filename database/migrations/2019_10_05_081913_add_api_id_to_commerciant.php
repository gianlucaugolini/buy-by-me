<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiIdToCommerciant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('commerciant', function ( Blueprint $table ) {
            $table->string( 'api_id' )->nullable( true )->default( null );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('commerciant', function ( Blueprint $table ) {
            $table->dropColumn('api_id' );
        });
    }
}
