<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('shop', function ( Blueprint $table ) {
            $table->increments('id');
            $table->integer( 's_commerciant_ext' )->unsigned();
            $table->tinyInteger( 's_blocked' )->unsigned()->default( 0 );
            $table->string( 's_name', 128 );
            $table->string( 's_image_name', 191 )->nullable( true )->default( null );
            $table->string( 's_image', 191 )->nullable( true )->default( null );
            $table->string( 's_image_real_path', 191 )->nullable( true )->default( null );
            $table->string( 's_image_path', 191 )->nullable( true )->default( null );
            $table->text( 's_desc' )->nullable( true );
            $table->string( 's_prd', 255 )->nullable( true );
            $table->string( 's_address', 128 )->nullable( true );
            $table->string( 's_cap', 5 )->nullable( true );
            $table->string( 's_city', 128 )->nullable( true );
            $table->string( 's_prov', 50 )->nullable( true );
            $table->string( 's_prov_code', 2 )->nullable( true );
            $table->string( 's_lat', 16 )->nullable( true );
            $table->string( 's_lon', 16 )->nullable( true );
            $table->integer( 's_cat' )->default( 0 );
            $table->integer( 's_catena_commerciale_ext' )->nullable()->default( null )->unsigned();
            $table->integer( 's_centro_commerciale_ext' )->nullable()->default( null )->unsigned();
            $table->string( 's_centro_commerciale_piano', 32 )->nullable( true );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('shop' );
    }
}
