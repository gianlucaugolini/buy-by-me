<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOffersTableAddFullPriceColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('offer', function ( Blueprint $table ) {
            $table->float( 'o_pric_start' )->default( 0 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('offer', function ( Blueprint $table ) {
            $table->dropColumn('o_pric_start' );
        });
    }
}
