<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCommerciantsAddSubscriptionExpiration extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('commerciant', function ( Blueprint $table ) {
            $table->timestamp( 'c_subscription' )->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('commerciant', function ( Blueprint $table ) {
            $table->dropColumn('c_subscription' );
        });
    }
}
