<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiPasswordResetTokenColumnToUsers extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('users', function ( Blueprint $table ) {
            $table->string('api_password_reset_token', 64 )->default( null )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('users', function ( Blueprint $table ) {
            $table->dropColumn('api_password_reset_token' );
        });
    }
}
