<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComerciantsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('commerciant', function (Blueprint $table) {
            $table->increments( 'id' );
            $table->integer( 'user_id' )->unsigned();
            $table->string( 'c_name', 128 )->nullable( true );
            $table->string( 'c_last_name', 128 )->nullable( true );
            $table->tinyInteger( 'c_confirmed' )->default( 0 );
            $table->tinyInteger( 'c_blocked' )->default( 0 );
            $table->string( 'c_token', 128 )->nullable( true );
            $table->integer( 'c_offers_bought' )->unsigned()->default( 0 );
            $table->integer( 'c_offers_used' )->unsigned()->default( 0 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('commerciant');
    }
}
