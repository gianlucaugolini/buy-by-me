<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('offer', function ( Blueprint $table ) {
            $table->timestamp( 'o_cancelled' )->nullable( true )->default( null );
            $table->integer( 'o_likes' )->unsigned()->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('offer', function ( Blueprint $table ) {
            $table->dropColumn('o_cancelled' );
            $table->dropColumn('o_likes' );
        });
    }
}
