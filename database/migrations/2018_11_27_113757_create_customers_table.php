<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('customer', function ( Blueprint $table ) {
            $table->increments( 'id' );
            $table->integer( 'user_id' )->unsigned();
            $table->string( 'u_name', 128 )->nullable( true );
            $table->string( 'u_last_name', 128 )->nullable( true );
            $table->string( 'u_prov', 50 )->nullable( true );
            $table->string( 'u_prov_code', 2 )->nullable( true );
            $table->tinyInteger( 'u_blocked' )->default( 0 );
            $table->tinyInteger( 'u_confirmed' )->default( 0 );
            $table->string( 'u_token', 255 )->nullable( true );
            $table->string( 'u_notification_ios_token', 512 )->nullable( true );
            $table->string( 'u_notification_android_token', 512 )->nullable( true );
            $table->tinyInteger( 'u_notification_ios_on' )->default( 0 );
            $table->tinyInteger( 'u_notification_android_on' )->default( 0 );
            $table->string( 'u_notification_provincia', 2 )->nullable( true );
            $table->tinyInteger( 'u_notification_only_filters' )->default( 0 );
            $table->string( 'u_notification_discounts', 18 )->default( '1;1;1;1;1;1;1;1;1;' );
            $table->string( 'u_notification_categories', 30 )->default( '1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;' );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists( 'customer' );
    }
}
