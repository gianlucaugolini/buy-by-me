<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditCustomerRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name'    =>  'required|max:128',
            'last_name'     =>  'max:128',
            'email'         =>  'required|max:191|unique:users,email,'.$this->user_id.',id',
            'prov'          =>  'required|max:50',
            'prov_code'     =>  'required|size:2|alpha',
        ];
    }
}
