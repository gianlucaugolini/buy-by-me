<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditOfferRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'o_title'           =>  'required|max:255',
            'o_desc'            =>  '',
            'o_pric_start'      =>  'numeric|between:0,999999.99|gte:o_pric',
            'o_pric'            =>  'required|numeric|between:0,999999.99',
            'o_disc'            =>  'numeric|between:0,100.00',
            'o_howm'            =>  'numeric|min:0',
            'o_start'           =>  'required|date_format:Y/m/d H:i:s',
            'o_end'             =>  'required|date_format:Y/m/d H:i:s',
            'o_cat'             =>  'numeric|exists:category,id',
        ];
    }
}
