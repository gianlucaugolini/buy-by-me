<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferTradeRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'commerciant_id'    =>  'required|numeric|exists:commerciant,id',
            'c_offers_bought'   =>  'required|numeric|min:0',
            'c_offers_used'     =>  'required|numeric|min:0'
        ];
    }
}
