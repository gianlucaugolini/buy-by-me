<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditShopRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'shop_id'                       =>  'required|numeric',
            's_name'                        =>  'required|max:128',
            's_phone'                       =>  'required|max:24',
            's_web'                         =>  'nullable|url|max:64',
            's_desc'                        =>  '',
            's_prd'                         =>  'max:255',
            's_address'                     =>  'max:128',
            's_cap'                         =>  'nullable|regex:/^[0-9]{5}$/','size:5',
            's_city'                        =>  'required|max:128',
            's_prov'                        =>  'required|max:50',
            's_prov_code'                   =>  'required|size:2|alpha',
            's_lat'                         =>  'max:16',
            's_lon'                         =>  'max:16',
            's_cat'                         =>  'numeric|exists:category,id',
            's_catena_commerciale_ext'      =>  'nullable|numeric|exists:catena_commerciale,id',
            's_centro_commerciale_ext'      =>  'nullable|numeric|exists:centro_commerciale,id',
            's_centro_commerciale_piano'    =>  'max:32',
        ];
    }

}
