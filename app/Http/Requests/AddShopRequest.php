<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddShopRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [

            'first_name'                    =>  'required|max:128',
            'last_name'                     =>  'required|max:128',
            'subscription'                  =>  'nullable|date_format:Y/m/d H:i:s|after:now',
            'email'                         =>  'required|max:191|unique:users,email',
            's_name'                        =>  'required|max:128',
            's_desc'                        =>  '',
            's_phone'                       =>  'required|max:24',
            's_web'                         =>  'nullable|url|max:64',
            's_prd'                         =>  'max:255',
            's_address'                     =>  'max:128',
            's_cap'                         =>  'nullable|regex:/^[0-9]{5}$/','size:5',
            's_city'                        =>  'max:128',
            's_prov'                        =>  'max:50',
            's_prov_code'                   =>  'max:2',
            's_lat'                         =>  'max:16',
            's_lon'                         =>  'max:16',
            's_cat'                         =>  'numeric|exists:category,id',
            's_catena_commerciale_ext'      =>  'nullable|numeric|exists:catena_commerciale,id',
            's_centro_commerciale_ext'      =>  'nullable|numeric|exists:centro_commerciale,id',
            's_centro_commerciale_piano'    =>  'max:32',
        ];
    }
}
