<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( $request, Closure $next ) {
        $is_admin = Auth::user()->type_id === User::ADMINISTRATOR;

        if ( !$is_admin ) {
            Auth::logout();
            $request->session()->invalidate();
            return redirect()->route('login');
        }

        return $next($request);
    }
}
