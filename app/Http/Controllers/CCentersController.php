<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddCCenterRequest;
use App\Http\Requests\EditCCenterRequest;
use App\Models\Catena;
use App\Models\CentroCommerciale;
use App\Models\Supervisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Shop;

class CCentersController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $ccenters = CentroCommerciale::paginate( config('constants.options.options_pagination') );

        return view( 'ccenters.list', compact(  'ccenters' ) );

    }

    public function details( $id ) {

        $ccenter = CentroCommerciale::withCount( 'shops' )->findOrFail( $id );

        return view( 'ccenters.details', compact( 'ccenter' ) );
    }

    /**
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $ccenter = CentroCommerciale::findOrFail( $id );

        return view('ccenters.forms.edit-ccenter', compact( 'ccenter' ) );
    }

    /**
     *
     * @param EditCCenterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditCCenterRequest $request ) {

        $fields = $request->all();

        $ccenter = CentroCommerciale::findOrFail( $fields[ 'mall_id' ] );


        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $ccenter->cc_name       = $fields[ 'cc_name' ];

            $ccenter->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.ccenter_edit_page.edit_ok' ) );
            return redirect()->route( 'center-details', [ 'id' => $ccenter->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.ccenter_edit_page.edit_ko' ) );
            return redirect()->route( 'center-details', [ 'id' => $ccenter->id ] );

        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function new() {

        return view('ccenters.forms.add-ccenter' );
    }

    /**
     *
     * @param AddCCenterRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create( AddCCenterRequest $request ) {

        $fields = $request->all();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $ccenter = new CentroCommerciale();

            $ccenter->cc_name = $fields[ 'cc_name' ];
            $ccenter->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.ccenter_add_page.add_ok' ) );
            return redirect()->route( 'center-details', [ 'id' => $ccenter->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.ccenter_add_page.add_ko' ) );
            return redirect()->route( 'all-centers' );

        }

    }

    public function delete( $id ) {

        try { // tenta la rimozione

            $ccenter = CentroCommerciale::findOrFail( $id );

            // inizializza una transazione
            DB::beginTransaction();

            Shop::where( [ 's_centro_commerciale_ext' => $id ] )->update(['s_centro_commerciale_ext' => null]);

            $ccenter->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.ccenter_deletion.deletion_ok' ) );
            return redirect()->route( 'all-centers' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.ccenter_deletion.deletion_ko' ) );
            return redirect()->route( 'center-details', [ 'id' => $id ] );

        }
    }

    public function export($dieScript = true){
        $records = CentroCommerciale::get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach( function ( $record ) {
            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
        } );

        $columns = [
            'id',
            'cc_name',
            'parsed_created_at',
            'parsed_updated_at',
        ];

        $csvExporter->build( $records, $columns )->download('centricommerciali.csv' );

       if ($dieScript) exit();
    }

}
