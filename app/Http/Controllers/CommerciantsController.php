<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditCommerciantRequest;
use App\Http\Requests\OfferTradeRequest;
use App\Models\Commerciant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommerciantsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $title = 'Modifica Cliente';
        $user = User::findOrFail( $id );

        if( $user->type_id !== User::COMMERCIANT ) {
            session()->flash( 'error', __('labels.user_edit.not_a_commerciant' ) );
            return redirect()->route( 'home' );
        }

        $commerciant_details = Commerciant::byUserId( $id )->firstOrFail();

        return view('users.forms.commerciant.edit-commerciant', compact( 'title', 'user', 'commerciant_details' ) );
    }

    /**
     *
     * @param EditCommerciantRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditCommerciantRequest $request ) {

        $fields = $request->all();

        // dd( $fields );

        $user = User::findOrFail( $fields[ 'user_id' ] );

        if( $user->type_id !== User::COMMERCIANT ) {
            session()->flash( 'error', __('labels.user_edit.not_a_customer' ) );
            return redirect()->route( 'home' );
        }

        $commerciant_details = Commerciant::byUserId( $fields[ 'user_id' ] )->firstOrFail();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->email = $fields[ 'email' ];
            $user->save();

            $commerciant_details->c_name = $fields[ 'first_name' ];
            $commerciant_details->c_last_name = $fields[ 'last_name' ];
            $commerciant_details->c_subscription = is_null( $fields[ 'c_subscription' ] ) ? null : Carbon::parse( $fields[ 'c_subscription' ] );

            $commerciant_details->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_edit.edit_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_edit.edit_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        }
    }

    public function offersTrade( $id ) {
        $commerciant = Commerciant::findOrFail( $id );

        return view('commerciant.forms.offers-trade', compact( 'commerciant' ) );

    }

    public function offersTradeSave( OfferTradeRequest $request ) {

        $fields = $request->all();


        $commerciant = Commerciant::findOrFail( $fields[ 'commerciant_id' ] );

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $commerciant->c_offers_bought = $fields[ 'c_offers_bought' ];
            $commerciant->c_offers_used = $fields[ 'c_offers_used' ];

            $commerciant->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.offers_trade_page.save_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $commerciant->user_id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.offers_trade_page.save_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $commerciant->user_id ] );

        }

    }

    public function export($dieScript = true){
        $records = Commerciant::with( [
            'user','shop'
        ] )->get();
        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach( function ( $record ) {
            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
            $record->email = $record->user->email;
            $record->fullname = $record->user->fullname;
            $record->details = "https://admin1.buybyme.net/public/users/details/".$record->id;
            $record->shop_details = "https://admin1.buybyme.net/public/shops/details/".$record->shop->id;
            $record->shop_name = $record->shop->s_name;

        });

        $columns = [
            'id',
            'details',
            'shop_name',
            'shop_details',
            'user_id',
            'c_name',
            'c_last_name',
            'c_confirmed',
            'c_blocked',
            'c_offers_bought',
            'c_offers_used',
            'email',
            'fullname',
            'parsed_created_at',
            'parsed_updated_at',
        ];


        $csvExporter->build( $records, $columns )->download('commercianti.csv' );

        if ($dieScript) exit();
    }

    public function deleteAllData( $id ) {

        $commerciant = Commerciant::with( [
            'user',
            'shop' => function( $q ) {
                $q->with( [
                    'offerte.favs'
                ] );
            }
        ] )->findOrFail( $id );


        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            foreach ( $commerciant->shop->offerte as $o => $offerta ) {
                $offerta->favs()->delete();
            }


            $commerciant->shop->offerte()->delete();
            $commerciant->shop->delete();
            $commerciant->user->delete();
            $commerciant->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.commerciant_deletion.deletion_ok' ) );
            return redirect()->route( 'shop-owners' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.commerciant_deletion.deletion_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $commerciant->user_id ] );

        }
    }

}
