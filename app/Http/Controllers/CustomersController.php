<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditCustomerRequest;
use App\Models\Commerciant;
use App\Models\Customer;
use App\Models\CustomerFav;
use App\Models\Shop;
use App\Models\Supervisor;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class CustomersController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth' );
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function customerFavs( $id ) {
        $title = 'Fav Utente';
        $user = User::findOrFail( $id );

        $customer_favs = CustomerFav::byCustomer( $id )->with( [ 'offer.shop' ] )->paginate( config('constants.options.options_pagination') );

        return view('customer.favs', compact( 'title', 'user', 'customer_favs' ) );
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $title = 'Modifica Cliente';
        $user = User::findOrFail( $id );

        if( $user->type_id !== User::CUSTOMER ) {
            session()->flash( 'error', __('labels.user_edit.not_a_customer' ) );
            return redirect()->route( 'home' );
        }

        $customer_details = Customer::byUserId( $id )->firstOrFail();

        return view('users.forms.customers.edit-customer', compact( 'title', 'user', 'customer_details' ) );
    }

    /**
     *
     * @param EditCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditCustomerRequest $request ) {

        $fields = $request->all();

        $user = User::findOrFail( $fields[ 'user_id' ] );

        if( $user->type_id !== User::CUSTOMER ) {
            session()->flash( 'error', __('labels.user_edit.not_a_customer' ) );
            return redirect()->route( 'home' );
        }

        $customer_details = Customer::byUserId( $fields[ 'user_id' ] )->firstOrFail();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->email = $fields[ 'email' ];
            $user->save();

            $customer_details->u_name = $fields[ 'first_name' ];
            $customer_details->u_last_name = $fields[ 'last_name' ];
            $customer_details->u_prov = $fields[ 'prov' ];
            $customer_details->u_prov_code = strtoupper( $fields[ 'prov_code' ] );
            $customer_details->u_notification_ios_on = 1;
            $customer_details->u_notification_android_on = 1;

            $customer_details->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_edit.edit_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_edit.edit_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        }
    }

    public function export($dieScript = true){
        $records = Customer::with( [
            'user'
        ] )->get();
        $csvExporter = new \Laracsv\Export();

        $csvExporter->beforeEach( function ( $record ) {

            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
            $record->email = $record->user->email;
            $record->email_verified_at = date( 'd/m/Y H:i:s', $record->user->email_verified_at );
        });

        $columns = [
            'id',
            'user_id',
            'email',
            'email_verified_at',
            'u_name',
            'u_last_name',
            'u_prov',
            'u_prov_code',
            'u_blocked',
            'u_confirmed',
            'u_token',
            'u_notification_ios_token',
            'u_notification_android_token',
            'u_notification_ios_on',
            'u_notification_android_on',
            'u_notification_provincia',
            'u_notification_only_filters',
            'u_notification_discounts',
            'u_notification_categories',
            'parsed_created_at',
            'parsed_updated_at',
        ];


        $csvExporter->build( $records, $columns )->download('clienti.csv' );

        if ($dieScript) exit();
    }
}
