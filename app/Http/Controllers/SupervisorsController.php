<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddSupervisorRequest;
use App\Http\Requests\EditCommerciantRequest;
use App\Http\Requests\EditSupervisorRequest;
use App\Models\Catena;
use App\Models\Commerciant;
use App\Models\Supervisor;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SupervisorsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function new() {

        $cc_list = [ '' => 'Selezionare una Catena Commerciale' ] + Catena::pluck( 'cc_name','id' )->toArray();

        return view('users.forms.supervisor.add-supervisor',compact( 'cc_list' ) );
    }

    /**
     *
     * @param AddSupervisorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create( AddSupervisorRequest $request ) {

        $fields = $request->all();


        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user = new User;

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->password =  bcrypt($fields[ 'password']); //bcrypt( 'test1234' );
            // $user->password = bcrypt( str_random(10) );
            $user->activation_token = null;
            $user->email_verified_at = Carbon::now();
            $user->email = $fields[ 'email' ];
            $user->type_id = User::SUPERVISOR;
            $user->save();

            $supervisor_details = new Supervisor;


            $supervisor_details->ccs_name = $fields[ 'first_name' ];
            $supervisor_details->ccs_last_name = $fields[ 'last_name' ];
            $supervisor_details->cc_ext         = $fields[ 'cc_ext' ] !== '' ? $fields[ 'cc_ext' ] : null;
            $supervisor_details->user_id = $user->id;

            $supervisor_details->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_add.add_supervisor_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_add.add_supervisor_ko' ) );
            return redirect()->route( 'supervisors' );

        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $title = trans( 'labels.user_edit.supervisor_edit_form_title' );
        $user = User::findOrFail( $id );

        if( $user->type_id !== User::SUPERVISOR ) {
            session()->flash( 'error', __('labels.user_edit.not_a_supervisor' ) );
            return redirect()->route( 'home' );
        }

        $supervisor_details = Supervisor::with( [ 'catena' ] )->byUserId( $id )->firstOrFail();

        if( !$supervisor_details->catena ) {
            $cc_list = [ '' => 'Selezionare una Catena Commerciale' ] + Catena::pluck( 'cc_name','id' )->toArray();
        } else {
            $cc_list = [];
        }

        return view('users.forms.supervisor.edit-supervisor', compact( 'title', 'user', 'supervisor_details', 'cc_list' ) );
    }

    /**
     *
     * @param EditSupervisorRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditSupervisorRequest $request ) {

        $fields = $request->all();

        $user = User::findOrFail( $fields[ 'user_id' ] );

        if( $user->type_id !== User::SUPERVISOR ) {
            session()->flash( 'error', __('labels.user_edit.not_a_supervisor' ) );
            return redirect()->route( 'home' );
        }

        $supervisor_details = Supervisor::byUserId( $fields[ 'user_id' ] )->firstOrFail();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->email = $fields[ 'email' ];
            if ($fields[ 'password' ] !== '') {
                $user->password = bcrypt($fields['password']);
            }
            $user->save();

            $supervisor_details->ccs_name       = $fields[ 'first_name' ];
            $supervisor_details->ccs_last_name  = $fields[ 'last_name' ];
            $supervisor_details->cc_ext         = $fields[ 'cc_ext' ] !== '' ? $fields[ 'cc_ext' ] : null;

            $supervisor_details->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_edit.edit_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_edit.edit_ko' ) . " i:".$e->getMessage());
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        }
    }

    public function removeFromMall( $id ) {

        $user = User::findOrFail( $id );

        if( $user->type_id !== User::SUPERVISOR ) {
            session()->flash( 'error', __('labels.user_edit.not_a_supervisor' ) );
            return redirect()->route( 'home' );
        }

        $supervisor_details = Supervisor::byUserId( $id )->firstOrFail();

        $supervisor_details->cc_ext = null;
        $updated = $supervisor_details->save();

        if ( $updated ) {
            session()->flash( 'success', __('labels.user_edit.remove_from_mall_ok' ) );
            return redirect()->route( 'edit-supervisor', [ 'id' => $user->id ] );
        } else {
            session()->flash( 'error', __('labels.user_edit.remove_from_mall_ko' ) );
            return redirect()->route( 'edit-supervisor', [ 'id' => $user->id ] );
        }
    }



    public function deleteAllData( $id ) {

        $supervisor = Supervisor::with( [
            'user',
            'catena'
        ] )->findOrFail( $id );

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $supervisor->user->delete();
            $supervisor->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.supervisor_deletion.deletion_ok' ) );
            return redirect()->route( 'supervisors' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.supervisor_deletion.deletion_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $supervisor->user_id ] );

        }
    }

}
