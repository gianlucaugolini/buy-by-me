<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddOfferImageRequest;
use App\Http\Requests\EditOfferRequest;
use App\Models\Category;
use App\Models\Catena;
use App\Models\Offer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OffersController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth' );
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $search = "";

        $filterLabel = "";

        $filter = "";

        $whereConditions = [];

        switch ($request->filter){
            case "running" :
                $whereConditions = [['o_end','>',Carbon::now()],['o_blocked','<>',1]];
                $filterLabel = "In corso";
                $filter = "running";
                break;
            case "ended" :
                $whereConditions = [['o_end','<=',Carbon::now()]];
                $filterLabel = "Terminate";
                $filter = "ended";
                break;
            case "blocked" :
                $whereConditions = [['o_blocked','<>',0]];
                $filterLabel = "Bloccate";
                $filter = "blocked";
                break;
        }

        if ($request->has('searchOffer')){
            $filterLabel .= $request->searchOffer;

            $search = $request->searchOffer;

            $whereConditions = [['o_title', 'like', '%' . $request->searchOffer . '%']];
        }

        $title = trans( 'labels.offers_list_title' );

        $offers = Offer::where($whereConditions)->orderBy('o_title', 'asc')->paginate(config('constants.options.options_pagination'));

        return view( 'offers.list', compact( 'title', 'offers', 'search','filter','filterLabel') );
    }

    public function offersearch(Request $request) {

        $search = "";

        $filterLabel = "";

        $filter = "";

        $whereConditions = [];

        switch ($request->filter){
            case "running" :
                $whereConditions = [['o_end','>',Carbon::now()],['o_blocked','<>',1]];
                $filterLabel = "In corso";
                $filter = "running";

                break;
            case "ended" :
                $whereConditions = [['o_end','<=',Carbon::now()]];
                 $filterLabel = "Terminate";
                 $filter = "ended";
                break;
            case "blocked" :
                $whereConditions = [['o_blocked','<>',0]];
                $filterLabel = "Bloccate";
                $filter = "blocked";
                break;
        }

        if ($request->has('searchOffer')){

            $search = $request->searchOffer;

            $whereConditions[] = ['o_title', 'like', '%' . $request->searchOffer . '%'];
        }

        $title = trans( 'labels.offers_list_title' );

        $offers = Offer::where($whereConditions)->orderBy('o_title', 'asc')->paginate(config('constants.options.options_pagination'));

        return view( 'offers.list', compact( 'title', 'offers', 'search','filter', 'filterLabel') );

    }

    public function details( $id ) {

        $title = trans( 'labels.offers_details_title' );

        $offer = Offer::with( [ 'shop', 'favedBy', 'categoria' ] )->withCount( 'favedBy' )->findOrFail( $id );

        return view( 'offers.details', compact( 'title', 'offer' ) );

    }

    function status( $offer_id, $status ) {

        $updated = Offer::where( [ 'id' => $offer_id ] )->update( [ 'o_blocked' => $status ] );

        if ( $updated ) {
            session()->flash( 'success', __('labels.offer_details_page.actions.status_update_ok' ) );
        } else {
            session()->flash( 'warning', __('labels.offer_details_page.actions.status_update_ok' ) );
        }

        return redirect()->route( 'offers-details', [ 'id' => $offer_id ] );

    }

    function cancel( $offer_id, $status ) {

        $updated = Offer::where( [ 'id' => $offer_id ] )->update( [ 'o_cancelled' => $status == 0 ? null : now() ] );

        if ( $updated ) {
            session()->flash( 'success', __('labels.offer_details_page.actions.status_update_ok' ) );
        } else {
            session()->flash( 'warning', __('labels.offer_details_page.actions.status_update_ok' ) );
        }

        return redirect()->route( 'offers-details', [ 'id' => $offer_id ] );

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {

        $offer = Offer::findOrFail( $id );

        $categories = Category::get();

        return view('offers.forms.edit-offer', compact( 'offer', 'categories' ) );
    }

    /**
     *
     * @param EditOfferRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditOfferRequest $request ) {

        $fields = $request->all();

        $offer = Offer::findOrFail( $fields[ 'offer_id' ] );

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $offer->o_title         =   $fields[ 'o_title' ];
            $offer->o_desc          =   $fields[ 'o_desc' ];
            $offer->o_pric_start    =   $fields[ 'o_pric_start' ];
            $offer->o_pric          =   $fields[ 'o_pric' ];
            $offer->o_disc          =   $fields[ 'o_disc' ];
            $offer->o_howm          =   $fields[ 'o_howm' ];
            $offer->o_start         =   Carbon::parse( $fields[ 'o_start' ] );
            $offer->o_end           =   Carbon::parse( $fields[ 'o_end' ] );
            $offer->o_cat           =   isset( $fields[ 'o_cat' ] ) ? $fields[ 'o_cat' ] : 0;

            $offer->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.offer_edit_page.edit_ok' ) );
            return redirect()->route( 'offers-details', [ 'id' => $offer->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.offer_edit_page.edit_ko' ) );
            return redirect()->route( 'offers-details', [ 'id' => $offer->id ] );

        }

    }

    public function uploadImage( $id ) {

        $offer = Offer::findOrFail( $id );

        return view('offers.forms.offer-image-upload', compact( 'offer' ) );
    }

    public function saveImage( AddOfferImageRequest $request ) {
        $fields = $request->all();

        $offer = Offer::findOrFail( $fields[ 'offer_id' ] );

        if ( $request->hasFile('offer_image') ) {

            if( file_exists( $offer->o_image_path ) ) {
                unlink( $offer->o_image_path );
            }


            $image = $request->file( 'offer_image' );

            $name = $offer->id . '.' . $image->getClientOriginalExtension();

            $image->storeAs('public/offer-images', $name );

            $offer->o_image_name = $name;
            $offer->o_image = 'storage/offer-images/' . $name;
            $offer->o_image_real_path = '/app/public/offer-images/' . $name;
            $offer->o_image_path = config( 'cross-storage.path' ) . '/offer-images/' . $name;
            $offer->save();

            $uploadSuccess = $image->move( config( 'cross-storage.path' ) .'/offer-images/', $name );

            session()->flash( 'success', __('labels.offer_add_image_page.offer_image_ok' ) );
            return redirect()->route( 'offers-details', [ 'id' => $offer->id ] );
        } else {

            session()->flash( 'success', __('labels.offer_add_image_page.offer_image_ko' ) );
            return redirect()->route( 'offers-details', [ 'id' => $offer->id ] );
        }


    }

    public function export($dieScript = true){
        $records = Offer::with( [ 'shop', 'categoria' ] )->get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach( function ( $record ) {
            $record->parsed_o_start = date( 'd/m/Y H:i:s', $record->o_start );
            $record->parsed_o_end = date( 'd/m/Y H:i:s', $record->o_end );
            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
            $record->dettagli = route( 'offers-details', [ 'id' => $record->id ] );
            $record->shop_details = route( 'shop-details', [ 'id' => $record->o_shop_ext ] );
        } );


        $columns = [
            'id',
            'o_blocked',
            'o_title',
            'o_image',
            'o_desc',
            'o_pric',
            'o_disc',
            'parsed_o_start',
            'parsed_o_end',
            'o_howm',
            'o_cat',
            'o_shop_ext',
            'o_uid',
            'o_views',
            'dettagli',
            'shop.s_name',
            'shop_details',
            'parsed_created_at',
            'parsed_updated_at',
        ];

        $csvExporter->build( $records, $columns )->download('offerte.csv' );

        if ($dieScript) exit();

    }
}
