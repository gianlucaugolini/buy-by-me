<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddShopImageRequest;
use App\Http\Requests\AddShopRequest;
use App\Http\Requests\EditShopRequest;
use App\Models\Catena;
use App\Models\CentroCommerciale;
use App\Models\Commerciant;
use App\Models\Offer;
use App\Models\Shop;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class ShopsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {

        $asc = true;

        if ($request->has('desc') && strlen(trim($request->desc))>0){
            $asc = false;
        }

        $field = "created_at";

        if ($request->has('order') && strlen(trim($request->order))>0){
            $field = trim($request->order);
        }

        $title = trans( 'labels.shops_list_title' );
        $shops = Shop::orderBy($field,  $asc ? 'asc' : 'desc')->paginate( config('constants.options.options_pagination') );

        return view( 'shops.list', compact( 'title', 'shops' ) );

    }

    public function catenacommerciale($id){
        $title = trans( 'labels.shops_list_title' ) . " nella stessa CATENA COMMERCIALE";
        $shops = Shop::where( [ 's_catena_commerciale_ext' => $id ] )->paginate( config('constants.options.options_pagination') );

        return view( 'shops.list', compact( 'title', 'shops' ) );
    }

    public function centrocommerciale($id){
        $title = trans( 'labels.shops_list_title' ) . " nello stesso CENTRO COMMERCIALE";
        $shops = Shop::where( [ 's_centro_commerciale_ext' => $id ] )->paginate( config('constants.options.options_pagination') );

        return view( 'shops.list', compact( 'title', 'shops' ) );
    }

    public function details( $id ) {
        $title = trans( 'labels.shop_details_title' );
        $shop = Shop::with( 'owner','catena', 'categoria' )->findOrFail( $id );

        return view( 'shops.details', compact( 'title', 'shop' ) );
    }

    function status( $shop_id, $status ) {

        $updated = Shop::where( [ 'id' => $shop_id ] )->update( [ 's_blocked' => $status ] );

        if ( $updated ) {
            session()->flash( 'success', __('labels.shop_details_page.actions.status_update_ok' ) );
        } else {
            session()->flash( 'warning', __('labels.shop_details_page.actions.status_update_ok' ) );
        }

        return redirect()->route( 'shop-details', [ 'id' => $shop_id ] );

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function new() {

        $title = 'Nuovo Negozio';

        $cc_list = Catena::pluck( 'cc_name','id' )->toArray();
        $c_list = CentroCommerciale::pluck( 'cc_name','id' )->toArray();

        $categories = Category::get();

        return view('shops.forms.add-shop', compact( 'title', 'cc_list', 'c_list', 'categories' ) );
    }

    /**
     *
     * @param AddShopRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create( AddShopRequest $request ) {

        $fields = $request->all();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user = new User;

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->password = bcrypt( 'test1234' );
            // $user->password = bcrypt( str_random(10) );
            $user->email = $fields[ 'email' ];
            $user->type_id = User::COMMERCIANT;
            $user->save();

            $commerciant_details = new Commerciant;


            $commerciant_details->c_name = $fields[ 'first_name' ];
            $commerciant_details->c_last_name = $fields[ 'last_name' ];
            $commerciant_details->user_id = $user->id;
            $commerciant_details->c_subscription = is_null( $fields[ 'subscription' ] ) ? null : Carbon::parse( $fields[ 'subscription' ] );

            $commerciant_details->save();

            $shop = new Shop;

            $shop->s_commerciant_ext            = $commerciant_details->id;
            $shop->s_name                       = $fields[ 's_name' ];
            $shop->s_desc                       = $fields[ 's_desc' ];
            $shop->s_phone                      = $fields[ 's_phone' ];
            $shop->s_web                        = $fields[ 's_web' ] ? $fields[ 's_web' ] : '';
            $shop->s_prd                        = $fields[ 's_prd' ];
            $shop->s_address                    = $fields[ 's_address' ];
            $shop->s_cap                        = $fields[ 's_cap' ];
            $shop->s_city                       = $fields[ 's_city' ];
            $shop->s_prov                       = $fields[ 's_prov' ];
            $shop->s_prov_code                  = $fields[ 's_prov_code' ];
            $shop->s_lat                        = $fields[ 's_lat' ];
            $shop->s_lon                        = $fields[ 's_lon' ];
            $shop->s_cat                        = isset( $fields[ 's_cat' ] ) ? $fields[ 's_cat' ] : 0;
            $shop->s_catena_commerciale_ext     = $fields[ 's_catena_commerciale_ext' ];
            $shop->s_centro_commerciale_ext     = $fields[ 's_centro_commerciale_ext' ];
            $shop->s_centro_commerciale_piano   = $fields[ 's_centro_commerciale_piano' ];
            $shop->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.shop_add_page.add_ok' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.shop_add_page.add_ko' ) );
            return redirect()->route( 'all-shops' );

        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {

        $title = 'Modifica Negozio';

        $shop = Shop::findOrFail( $id );

        $cc_list = Catena::pluck( 'cc_name','id' )->toArray();
        $c_list = CentroCommerciale::pluck( 'cc_name','id' )->toArray();

        $categories = Category::get();

        return view('shops.forms.edit-shop', compact( 'title', 'shop', 'cc_list',  'c_list', 'categories' ) );
    }

    /**
     *
     * @param EditShopRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditShopRequest $request ) {

        $fields = $request->all();

        try { // tenta l'inserimento
            $shop = Shop::findOrFail( $fields[ 'shop_id' ] );

            // inizializza un transazione
            DB::beginTransaction();

            $shop->s_name                       = $fields[ 's_name' ];
            $shop->s_desc                       = $fields[ 's_desc' ];
            $shop->s_phone                      = $fields[ 's_phone' ];
            $shop->s_web                        = $fields[ 's_web' ] ? $fields[ 's_web' ] : '';
            $shop->s_prd                        = $fields[ 's_prd' ];
            $shop->s_address                    = $fields[ 's_address' ];
            $shop->s_cap                        = $fields[ 's_cap' ];
            $shop->s_city                       = $fields[ 's_city' ];
            $shop->s_prov                       = $fields[ 's_prov' ];
            $shop->s_prov_code                  = $fields[ 's_prov_code' ];
            $shop->s_lat                        = $fields[ 's_lat' ];
            $shop->s_lon                        = $fields[ 's_lon' ];
            $shop->s_cat                        = $fields[ 's_cat' ];
            $shop->s_catena_commerciale_ext     = $fields[ 's_catena_commerciale_ext' ];
            $shop->s_centro_commerciale_ext     = $fields[ 's_centro_commerciale_ext' ];
            $shop->s_centro_commerciale_piano   = $fields[ 's_centro_commerciale_piano' ];
            $shop->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.shop_edit_page.edit_ok' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.shop_edit_page.edit_ko' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );

        }

    }

    public function offers( $id ) {

        $offers = Offer::byShop( $id )->orderBy("created_at", 'desc')->paginate( config('constants.options.options_pagination'));

        $shop = Shop::findOrFail( $id );

        $title = trans( 'labels.shop_offers.offers_list_title', [ 'shop_name' => $shop->s_name ] );

        return view( 'shops.offers.list', compact( 'title', 'offers' ) );

    }


    public function uploadImage( $id ) {

        $shop = Shop::findOrFail( $id );

        return view('shops.forms.shop-image-upload', compact( 'shop' ) );
    }

    public function saveImage( AddShopImageRequest $request ) {

        // dd( $_SERVER['DOCUMENT_ROOT'] );
        $fields = $request->all();

        $shop = Shop::findOrFail( $fields[ 'shop_id' ] );

        if ( $request->hasFile('shop_image') ) {

            if( $shop->s_image_path ) {
                if( file_exists( $shop->s_image_path ) ) {
                    unlink( $shop->s_image_path );
                }
            }

            if( $shop->s_image_real_path ) {
                if( file_exists( storage_path() . $shop->s_image_real_path ) ) {
                    unlink( storage_path() . $shop->s_image_real_path );
                }
            }

            $image = $request->file( 'shop_image' );

            $name = $shop->id . '.' . $image->getClientOriginalExtension();

            $image->storeAs( 'public/shop-images', $name );

            $shop->s_image_name = $name;
            $shop->s_image = 'public/storage/shop-images/' . $name;
            $shop->s_image_real_path = '/app/public/shop-images/' . $name;
            $shop->s_image_path = config( 'cross-storage.path' ) . '/shop-images/' . $name;

            $shop->save();
            $uploadSuccess = $image->move( config( 'cross-storage.path' ) .'/shop-images/', $name );


            session()->flash( 'success', __('labels.shop_add_image_page.shop_image_ok' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );
        } else {

            session()->flash( 'success', __('labels.shop_add_image_page.shop_image_ko' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );
        }
    }

    public function export($dieScript = true){
        $records = Shop::get();

        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach(function ($record) {
            $shop = Shop::with( 'owner','catena', 'categoria', 'centroCommerciale' )->findOrFail( $record->id );
            $user = User::findOrFail( $shop->owner->user_id);

            $record->s_cat_name = $record->categoria->name;
            $record->dettagli = route( 'shop-details', [ 'id' => $record->id ] );
            $record->owner_email = $user->email;
            $record->owner_details = route( 'user-details', [ 'id' => $user->id ] );

            $record->catena = $shop->catena ? $shop->catena->cc_name : "";
            $record->centroCommerciale = $shop->centroCommerciale ? $shop->centroCommerciale->cc_name : "";

            if ($record->s_catena_commerciale_ext!==null && $record->s_catena_commerciale_ext !==0) {
                $record->s_catena_commerciale_ext = route('mall-details', ['id' => $record->s_catena_commerciale_ext]);
            }
            else {
                $record->s_catena_commerciale_ext = "";
            }

            if ($record->s_centro_commerciale_ext!==null && $record->s_centro_commerciale_ext !==0) {
                $record->s_centro_commerciale_ext = route('center-details', ['id' => $record->s_centro_commerciale_ext]);
            }
            else {
                $record->s_centro_commerciale_ext = "";
            }

            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
        });

        $columns = [
            'id',
            's_commerciant_ext',
            's_blocked',
            's_name',
            's_image',
            's_desc',
            's_prd',
            's_address',
            's_cap',
            's_city',
            's_prov',
            's_prov_code',
            's_lat',
            's_lon',
            's_cat',
            's_catena_commerciale_ext',
            's_centro_commerciale_ext',
            's_centro_commerciale_piano',
            's_cat_name',
            'dettagli',
            'owner.c_name',
            'owner.c_last_name',
            'owner_email',
            'owner_details',
            'catena',
            'centroCommerciale',
            'parsed_created_at',
            'parsed_updated_at',
        ];


        //$columns[] = "s_centro_commerciale_name";
        //$columns[] = "s_centro_commerciale";
        //$columns[] = "s_catena_commerciale_name";
        //$columns[] = "s_catena_commerciale";

        $csvExporter->build( $records,$columns )->download('shops.csv' );

        if ($dieScript) exit();
    }


    public function deleteAllData( $id ) {

        $shop = Shop::with( [
            'owner.user',
            'offerte.favs'
        ] )->findOrFail( $id );


        try { // tenta l'inserimento

            // inizializza una transazione
            DB::beginTransaction();

            foreach ( $shop->offerte as $o => $offerta ) {
                $offerta->favs()->delete();
            }


            $shop->offerte()->delete();
            $shop->owner->user->delete();
            $shop->owner->delete();
            $shop->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.shop_deletion.deletion_ok' ) );
            return redirect()->route( 'all-shops' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.shop_deletion.deletion_ko' ) );
            return redirect()->route( 'shop-details', [ 'id' => $shop->id ] );

        }
    }

}
