<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddAdminRequest;
use App\Http\Requests\EditAdminRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\ShopsController;
use App\Http\Controllers\CCentersController;
use App\Http\Controllers\OffersController;
use App\Http\Controllers\MallsController;

class AdminsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function new() {

        return view('users.forms.admin.add-admin' );
    }

    /**
     *
     * @param AddAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create( AddAdminRequest $request ) {

        $fields = $request->all();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user = new User;

            $user->fullname = $fields[ 'first_name' ] . ' ' . $fields[ 'last_name' ];
            $user->password = bcrypt( 'test1234' );
            // $user->password = bcrypt( str_random(10) );
            $user->email = $fields[ 'email' ];
            $user->type_id = User::ADMINISTRATOR;
            $user->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_add.add_admin_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_add.add_admin_ko' ) );
            return redirect()->route( 'administrators' );

        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {

        if( !auth()->user()->is_superadmin ) {
            return redirect()->route( 'home' );
        }

        $title = trans( 'labels.user_edit.admin_edit_form_title' );
        $user = User::findOrFail( $id );

        if( $user->type_id !== User::ADMINISTRATOR ) {
            session()->flash( 'error', __('labels.user_edit.not_an_admin' ) );
            return redirect()->route( 'home' );
        }

        return view('users.forms.admin.edit-admin', compact( 'title', 'user' ) );
    }

    /**
     *
     * @param EditAdminRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditAdminRequest $request ) {

        if( !auth()->user()->is_superadmin ) {
            return redirect()->route( 'home' );
        }

        $fields = $request->all();

        $user = User::findOrFail( $fields[ 'user_id' ] );

        if( $user->type_id !== User::ADMINISTRATOR ) {
            session()->flash( 'error', __('labels.user_edit.not_an_admin' ) );
            return redirect()->route( 'home' );
        }

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $user->fullname = $fields[ 'fullname' ];
            $user->email = $fields[ 'email' ];
            $user->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.user_edit.edit_ok' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.user_edit.edit_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $user->id ] );

        }
    }

    /**
     * Database Downloads
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download_backup (){
        $headers = array(
            'Content-Type' => 'application/sql',
        );
        return response()->download(storage_path("app/backup/buybyme.sql"),"buybyme".date('_d_m_Y_H_i_s').".sql",$headers);
    }

    public function download_partial_backup_csv(){
        $headers = array(
            'Content-Type' => 'application/sql',
        );
        return response()->download(storage_path("app/backup/buybyme_csv.zip"),"buybyme_csv".date('_d_m_Y_H_i_s').".zip",$headers);
    }
}
