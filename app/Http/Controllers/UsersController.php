<?php

namespace App\Http\Controllers;

use App\Models\Commerciant;
use App\Models\Customer;
use App\Models\Supervisor;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UsersController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('home', compact( 'title' ) );
    }


    public function administrators() {
        $title = trans( 'labels.users_list_titles.administrators' );

        $users = User::where( [
            'type_id' => User::ADMINISTRATOR
        ] )->orderBy('fullname', 'asc')->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title', 'users' ) );
    }

    public function customers(Request $request) {

        $order = "fullname";
        $asc = true;

        if ($request->has('order') && strlen(trim($request->order))!=0){
            $order = $request->order;
        }

        if ($request->has('desc') && strlen(trim($request->desc))!=0){
            $asc = false;
        }

        $whereConditions = [
            'type_id' => User::CUSTOMER
        ];

        $filterLabel = "";

        switch ($request->filter){
           case "blocked" :
               $whereConditions = [
                   'type_id' => User::CUSTOMER,
                   'u_blocked'=>1
               ];
               $filterLabel = "Bloccati";
               break;
           case "confirmed" :
               $whereConditions = [
                   'type_id' => User::CUSTOMER,
                   'u_confirmed'=>1
               ];
               $filterLabel = "Confermati";
               break;
        }

        $title = trans( 'labels.users_list_titles.customers' );
        $users = User::where( $whereConditions )
            ->leftJoin('customer', 'users.id', '=', 'customer.user_id')
            ->select(
                'users.id',
                'users.fullname',
                'users.email',
                'users.created_at',
                'customer.u_blocked',
                'customer.u_confirmed')
            ->orderBy($order, $asc ? 'asc' : 'desc')
            ->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title', 'users', 'filterLabel') );

    }

    public function commerciants(Request $request) {

        $order = "fullname";
        $asc = true;

        if ($request->has('order') && strlen(trim($request->order))!=0){
            $order = "users.".$request->order;
        }

        if ($request->has('desc') && strlen(trim($request->desc))!=0){
            $asc = false;
        }

        $title = trans( 'labels.users_list_titles.commerciants' );
        $whereConditions = [
            'type_id' => User::COMMERCIANT
        ];

        $filterLabel = "";

        switch ($request->filter){
            case "blocked" :
                $whereConditions = [
                    'type_id' => User::COMMERCIANT,
                    'c_blocked'=>1
                ];
                $filterLabel = "Bloccati";
                break;
            case "confirmed" :
                $whereConditions = [
                    'type_id' => User::COMMERCIANT,
                    'c_confirmed'=>1
                ];
                $filterLabel = "Confermati";
                break;
        }

        $users = User::where( $whereConditions )
            ->leftJoin('commerciant', 'users.id', '=', 'commerciant.user_id')
            ->select(
                'users.id',
                'users.fullname',
                'users.api_id',
                'users.email',
                'users.created_at',
                'commerciant.c_blocked',
                'commerciant.c_confirmed',
                'commerciant.c_subscription'
            )
            ->orderBy($order, $asc ? 'asc' : 'desc')
            ->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title', 'users', 'filterLabel') );
    }

    //SEARCH

    public function customersearch(Request $request) {

    if (!$request->has('searchUser') || strlen(trim($request->searchUser))==0){
            return $this->customers($request);
        }

        $order = "fullname";
        $asc = true;

        if ($request->has('order') && strlen(trim($request->order))!=0){
            $order = "users.".$request->order;
        }

        if ($request->has('desc') && strlen(trim($request->desc))!=0){
            $asc = false;
        }

        $filterLabel = "";

        $whereConditions = [
            'type_id' => User::CUSTOMER
        ];

        switch ($request->filter){
            case "blocked" :
                $whereConditions = [
                    'type_id' => User::CUSTOMER,
                    'u_blocked'=>1
                ];
                $filterLabel = "Bloccati";
                break;
            case "confirmed" :
                $whereConditions = [
                    'type_id' => User::CUSTOMER,
                    'u_confirmed'=>1
                ];
                $filterLabel = "Confermati";
                break;
        }

        $title = trans( 'labels.users_list_titles.customers' );
        $users = User::where( $whereConditions );
        $search = "";

        if ($request->has('searchUser')){
            $users = $users->where('users.fullname', 'like', '%' . $request->searchUser . '%')->orWhere('users.email', 'like', '%' . $request->searchUser . '%')->where($whereConditions);
            $search = $request->searchUser;
        }

        $users = $users->leftJoin('customer', 'users.id', '=', 'customer.user_id')
            ->select(
                'users.id',
                'users.fullname',
                'users.email',
                'users.created_at',
                'customer.u_blocked',
                'customer.u_confirmed')
            ->orderBy($order, $asc ? 'asc' : 'desc')
            ->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title','search' ,'users', 'filterLabel') );

    }

    public function commerciantsearch(Request $request) {

        if (!$request->has('searchUser') || strlen(trim($request->searchUser))==0){
            return $this->commerciants($request);
        }

        $order = "fullname";
        $asc = true;

        if ($request->has('order') && strlen(trim($request->order))!=0){
            $request->order;
        }

        if ($request->has('desc') && strlen(trim($request->desc))!=0){
            $asc = false;
        }

        $whereConditions = [
            'type_id' => User::COMMERCIANT
        ];

        $filterLabel = "";

        switch ($request->filter){
            case "blocked" :
                $whereConditions = [
                    'type_id' => User::COMMERCIANT,
                    'c_blocked'=>1
                ];
                $filterLabel = "Bloccati";
                break;
            case "confirmed" :
                $whereConditions = [
                    'type_id' => User::COMMERCIANT,
                    'c_confirmed'=>1
                ];
                $filterLabel = "Confermati";
                break;
        }

        $title = trans( 'labels.users_list_titles.commerciants' );
        $users = User::where( $whereConditions );
        $search = "";

        if ($request->has('searchUser')){
            $users = $users->where('users.fullname', 'like', '%' . $request->searchUser . '%')->orWhere('users.email', 'like', '%' . $request->searchUser . '%')->where($whereConditions);
            $search = $request->searchUser;
        }

        $users = $users->leftJoin('commerciant', 'users.id', '=', 'commerciant.user_id')
            ->select(
                'users.id',
                'users.fullname',
                'users.email',
                'users.created_at',
                'commerciant.c_offers_bought',
                'commerciant.c_offers_used',
                'commerciant.c_blocked',
                'commerciant.c_confirmed')
            ->orderBy($order, $asc ? 'asc' : 'desc')
            ->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title','search' ,'users', 'filterLabel') );

    }

    public function supervisors() {
        $title = trans( 'labels.users_list_titles.supervisors' );
        $users = Supervisor::with( 'user' ,'catena')
            ->orderBy('id', 'asc')
            ->paginate( config('constants.options.options_pagination') );

        return view( 'users.list', compact( 'title', 'users' ) );

    }



    public function deleteCustomer($id) {
        $user = User::findOrFail( $id );

        $consumer = Customer::byUserId( $id )->firstOrFail();

        try { // tenta la cancellazione

            // inizializza un transazione
            DB::beginTransaction();

            $consumer->favs()->delete();

            $consumer->delete();

            $user->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.consumer_deletion.deletion_ok' ) );
            return redirect()->route( 'customers' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.consumer_deletion.deletion_ko' ) );
            return redirect()->route( 'user-details', [ 'id' => $consumer->id ] );

        }
    }

    public function details( $id ) {
        $title = trans( 'labels.users_details_title' );
        $user = User::findOrFail( $id );

        switch ( $user->type_id ) {
            case User::ADMINISTRATOR :
                $user_details = [];
                break;
            case User::CUSTOMER :
                $user_details = Customer::byUserId( $id )
                    ->withCount( 'favs' )
                    ->firstOrFail();
                break;
            case User::COMMERCIANT :

                $user_details = Commerciant::byUserId( $id )
                    ->with( [ 'shop' ] )
                    ->firstOrFail();

                break;

            case User::SUPERVISOR :
                $user_details = Supervisor::byUserId( $id )
                    ->firstOrFail();
                break;
            default :
                return redirect( 'home' );
                break;
        }

        return view( 'users.details', compact( 'title', 'user', 'user_details' ) );

    }

    function status( $user_id, $status ) {
        $user = User::findOrFail( $user_id );


        $updated = false;

        switch ( $user->type_id ) {
            case User::ADMINISTRATOR :
                // TODO UPDATE ADMIN STATUS
                break;
            case User::CUSTOMER :

                $updated = Customer::byUserId( $user_id )
                    ->update( [ 'u_blocked' => $status ] );
                // dd( $updated );

                if ( $updated ) {
                    session()->flash( 'success', __('labels.user_details_page.actions.status_update_ok' ) );
                } else {
                    session()->flash( 'warning', __('labels.user_details_page.actions.status_update_ok' ) );
                }

                break;
            case User::COMMERCIANT :

                $updated = Commerciant::byUserId( $user_id )
                    ->update( [ 'c_blocked' => $status ] );

                if ( $updated ) {
                    session()->flash( 'success', __('labels.user_details_page.actions.status_update_ok' ) );
                } else {
                    session()->flash( 'warning', __('labels.user_details_page.actions.status_update_ok' ) );
                }

                break;
            case User::SUPERVISOR :

                $updated = Supervisor::byUserId( $user_id )
                    ->update( [ 'ccs_blocked' => $status ] );

                if ( $updated ) {
                    session()->flash( 'success', __('labels.user_details_page.actions.status_update_ok' ) );
                } else {
                    session()->flash( 'warning', __('labels.user_details_page.actions.status_update_ok' ) );
                }
                break;
            default :
                return redirect( 'home' );
                break;
        }

        return redirect()->route( 'user-details', [ 'id' => $user_id ] );

    }

    function confirmed( $user_id, $confirmed ) {
        $user = User::findOrFail( $user_id );

        $updated = false;

        switch ( $user->type_id ) {
            case User::ADMINISTRATOR :
                // NOT NEEDED
                break;
            case User::CUSTOMER :

                $updated = Customer::byUserId( $user_id )
                    ->update( [ 'u_confirmed' => $confirmed ] );
                // dd( $updated );

                if ( $updated ) {
                    session()->flash( 'success', __('labels.user_details_page.actions.status_update_ok' ) );
                } else {
                    session()->flash( 'warning', __('labels.user_details_page.actions.status_update_ok' ) );
                }

                break;
            case User::COMMERCIANT :

                $updated = Commerciant::byUserId( $user_id )
                    ->update( [ 'c_confirmed' => $confirmed ] );

                if ( $updated ) {
                    session()->flash( 'success', __('labels.user_details_page.actions.status_update_ok' ) );
                } else {
                    session()->flash( 'warning', __('labels.user_details_page.actions.status_update_ok' ) );
                }

                break;
            case User::SUPERVISOR :
                // NOT NEEDED
                 break;
            default :
                return redirect( 'home' );
                break;
        }

        return redirect()->route( 'user-details', [ 'id' => $user_id ] );

    }
}
