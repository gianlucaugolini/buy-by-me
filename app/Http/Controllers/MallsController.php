<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddMallRequest;
use App\Http\Requests\EditMallRequest;
use App\Models\Catena;
use App\Models\Supervisor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Shop;

class MallsController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $title = trans( 'labels.malls_list_title' );
        $malls = Catena::paginate( config('constants.options.options_pagination') );

        return view( 'malls.list', compact( 'title', 'malls' ) );

    }

    public function details( $id ) {

        $catena = Catena::with( 'supervisori' )->with( 'shops' )->findOrFail( $id );

        //$shops = Shop::where( [ 's_catena_commerciale_ext' => $catena->id ] );

        return view( 'malls.details', compact( 'catena' ) );
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function edit( $id ) {
        $mall = Catena::findOrFail( $id );

        return view('malls.forms.edit-mall', compact( 'mall' ) );
    }

    /**
     *
     * @param EditMallRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update( EditMallRequest $request ) {

        $fields = $request->all();

        $mall = Catena::findOrFail( $fields[ 'mall_id' ] );


        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();

            $mall->cc_name       = $fields[ 'cc_name' ];

            $mall->save();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.mall_edit_page.edit_ok' ) );
            return redirect()->route( 'mall-details', [ 'id' => $mall->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.mall_edit_page.edit_ko' ) );
            return redirect()->route( 'mall-details', [ 'id' => $mall->id ] );

        }
    }

    public function removeSupervisor( $ccid, $sid ) {
        $supervisor = Supervisor::byUserId( $sid )->firstOrFail();

        if( $supervisor->cc_ext == $ccid ) {
            $supervisor->cc_ext = null;
            $updated = $supervisor->save();

            if ( $updated ) {
                session()->flash( 'success', __('labels.mall_details_page.remove_from_mall_ok' ) );
                return redirect()->route( 'mall-details', [ 'id' => $ccid ] );
            } else {
                session()->flash( 'error', __('labels.mall_details_page.remove_from_mall_ko' ) );
                return redirect()->route( 'mall-details', [ 'id' => $ccid ] );
            }

        } else {
            session()->flash( 'error', __('labels.mall_details_page.remove_from_mall_warning' ) );
            return redirect()->route( 'mall-details', [ 'id' => $ccid ] );
        }

    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function new() {

        $supervisors_list = Supervisor::with( 'user' )->noSupervision()->get();

        $supervisors_select = [];

        foreach ( $supervisors_list as $sl => $supervisor ) {
            $supervisors_select[ $supervisor->id ] = $supervisor->user->fullname;
        }

        return view('malls.forms.add-mall', compact(  'supervisors_select' ) );
    }

    /**
     *
     * @param AddMallRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create( AddMallRequest $request ) {

        $fields = $request->all();

        try { // tenta l'inserimento

            // inizializza un transazione
            DB::beginTransaction();


            $mall = new Catena;

            $mall->cc_name = $fields[ 'cc_name' ];
            $mall->save();

            if( $fields[ 'supervisor_id' ]) {
                $supervisor = Supervisor::findOrFail( $fields[ 'supervisor_id' ] );
                $supervisor->cc_ext = $mall->id;
                $supervisor->save();
            }

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.mall_add_page.add_ok' ) );
            return redirect()->route( 'mall-details', [ 'id' => $mall->id ] );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.mall_add_page.add_ko' ) );
            return redirect()->route( 'all-malls' );

        }

    }

    public function delete( $id ) {

        try { // tenta l'la rimozione

            $mall = Catena::findOrFail( $id );

            // inizializza una transazione
            DB::beginTransaction();

            Shop::where( [ 's_catena_commerciale_ext' => $id ] )->update(['s_catena_commerciale_ext' => null]);

            Supervisor::where(['cc_ext'=>$id])->update(['cc_ext' => null]);

            $mall->delete();

            // committa la transazione
            DB::commit();

            session()->flash( 'success', __('labels.mall_deletion.deletion_ok' ) );
            return redirect()->route( 'all-malls' );

        } catch ( \Exception $e ) { // se genera eccezione

            // esegue un rollback sulla transazione
            DB::rollBack();

            session()->flash( 'error', __('labels.mall_deletion.deletion_ko' ) );
            return redirect()->route( 'mall-details', [ 'id' => $id ] );

        }
    }

    public function export($dieScript = true){
        $records = Catena::get();
        $csvExporter = new \Laracsv\Export();
        $csvExporter->beforeEach( function ( $record ) {
            $record->parsed_created_at = date( 'd/m/Y H:i:s', $record->created_at );
            $record->parsed_updated_at = date( 'd/m/Y H:i:s', $record->updated_at );
        } );

        $columns = [
            'id',
            'cc_name',
            'parsed_created_at',
            'parsed_updated_at',
        ];


        $csvExporter->build($records,$columns)->download("catenecommerciali.csv");

        if ($dieScript) exit();
    }
}
