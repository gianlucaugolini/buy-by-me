<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Commerciant extends Model {

    protected $table = 'commerciant';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'c_name',
        'c_last_name',
        'c_confirmed',
        'c_blocked',
        'c_token',
        'c_offers_bought',
        'c_offers_used',
        'c_subscription'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'        => 'timestamp',
        'updated_at'        => 'timestamp',
        'c_subscription'    => 'timestamp'
    ];

    /**
     * Lo Shop di cui il Commerciant è proprietario
     */
    public function shop() {
        return $this->hasOne('App\Models\Shop', 's_commerciant_ext', 'id' );
    }

    public function user() {
        return $this->belongsTo('App\Models\User' );
    }

    public function scopeNotBlocked( $query ) {
        return $query->where( 'c_blocked', '<>', 1 );
    }

    public function scopeByUserId( $query, $id ) {
        return $query->where( 'user_id', $id );
    }

    public function getBlockedLabelAttribute() {
        return $this->c_blocked === 0 ? 'NO' : 'SI';
    }

    public function getConfirmedLabelAttribute() {
        return $this->c_confirmed === 0 ? 'NO' : 'SI';
    }

    public static function getTableColumns() {
        list( $cols, $values ) = array_divide( ( new static )->first()->toArray() );
        return $cols;
    }

}
