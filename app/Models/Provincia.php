<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model {

    protected $table = 'provincias';
    protected $primaryKey = 'id'; // or null

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'prov',
        'prov_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at','updated_at'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function getTableColumns() {
    list($cols, $values) = array_divide((new static)->first()->toArray());
    return $cols;
}
}
