<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model {

    protected $table = 'shop';
    protected $primaryKey = 'id'; // or null

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        's_commerciant_ext',
        's_blocked',
        's_name',
        's_image_name',
        's_image',
        's_image_real_path',
        's_image_path',
        's_desc',
        's_prd',
        's_indirizzo',
        's_cap',
        's_city',
        's_prov',
        's_prov_code',
        's_lat',
        's_lon',
        's_cat',
        's_catena_commerciale_ext',
        's_centro_commerciale_ext',
        's_centro_commerciale_piano',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * La Catena commerciale a cui appartiere lo shop
     */
    public function catena() {
        return $this->belongsTo('App\Models\Catena', 's_catena_commerciale_ext', 'id' );
    }

    /**
     * Il Centro commerciale dove si trova lo shop
     */
    public function centroCommerciale() {
        return $this->belongsTo('App\Models\CentroCommerciale', 's_centro_commerciale_ext', 'id' );
    }

    /**
     * Le Offerte dello Shop
     */
    public function offerte() {
        return $this->hasMany('App\Models\Offer', 'o_shop_ext', 'id' );
    }

    public function owner() {
        return $this->belongsTo('App\Models\Commerciant', 's_commerciant_ext', 'id' );
    }

    public function categoria() {
        return $this->belongsTo('App\Models\Category', 's_cat', 'id' );
    }

    public function scopeNotBlocked( $query ) {
        return $query->where( 's_blocked', '<>', 1 );
    }

    public function getBlockedLabelAttribute() {
        return $this->s_blocked === 0 ? 'NO' : 'SI';
    }

    public static function getTableColumns() {
    list($cols, $values) = array_divide((new static)->first()->toArray());
    return $cols;
}
}
