<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Commerciant;

class User extends Authenticatable {
    use Notifiable;

    const ADMINISTRATOR = 1;
    const CUSTOMER = 2;
    const COMMERCIANT = 3;
    const SUPERVISOR = 4;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'password', 'type_id', 'activation_token', 'api_password_reset_token', 'logged'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'logged'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'        => 'timestamp',
        'updated_at'        => 'timestamp',
        'email_verified_at' => 'timestamp',
    ];

    public function scopeByTypeId( $query, $id ) {
        return $query->where( 'type_id', $id );
    }

    public function commerciant() {
        return $this->hasOne('App\Models\Commerciant');
    }

    public static function getTableColumns() {
        list($cols, $values) = array_divide((new static)->first()->toArray());
        return $cols;
    }
}
