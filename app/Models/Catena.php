<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Catena extends Model {

    protected $table = 'catena_commerciale';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cc_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];


    /**
     * Gli shop della Catena Commerciale
     */
    public function shops() {
        return $this->hasMany('App\Models\Shop', 's_catena_commerciale_ext', 'id' );
    }

    /**
     * I supervisori
     */
    public function supervisori() {
        return $this->hasMany('App\Models\Supervisor', 'cc_ext', 'id' );
    }

    public static function getTableColumns() {
    list($cols, $values) = array_divide((new static)->first()->toArray());
    return $cols;
}
}
