<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {

    protected $table = 'offer';
    protected $primaryKey = 'id'; // or null
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'o_blocked',
        'o_title',
        'o_image_name',
        'o_image',
        'o_image_real_path',
        'o_image_path',
        'o_desc',
        'o_pric',
        'o_pric_start',
        'o_disc',
        'o_howm',
        'o_start',
        'o_end',
        'o_cat',
        'o_shop_ext',
        'o_uid',
        'o_views',
        'o_cancelled',
        'o_likes',
    ];

    protected $dates = [
        'o_start',
        'o_end',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at'    => 'timestamp',
        'updated_at'    => 'timestamp',
        'o_start'       => 'timestamp',
        'o_end'         => 'timestamp',
    ];

    public function favedBy() {
        return $this->belongsToMany('App\Models\Customer','user_favs', 'uf_offerid_ext', 'uf_userid_ext');
    }

    public function likedBy() {
        return $this->belongsToMany('App\Models\Customer','users_likes', 'ul_offerid_ext', 'ul_userid_ext')->withTimestamps();
    }

    /**
     * Lo Shop che eroga l'offerta
     */
    public function shop() {
        return $this->belongsTo('App\Models\Shop', 'o_shop_ext', 'id' );
    }

    public function categoria() {
        return $this->belongsTo('App\Models\Category', 'o_cat', 'id' );
    }

    public function favs() {
        return $this->hasMany('App\Models\CustomerFav', 'uf_offerid_ext', 'id' );
    }

    public function scopeNotBlocked( $query ) {
        return $query->where( 'o_blocked', '<>', 1 );
    }

    public function scopeOnlyId( $query ) {
        // $this->makeHidden( 'pivot' );
        return $query->select( 'offer.id' )->without( 'pivot' );
    }

    public function scopeByShop( $query, $id ) {
        return $query->where( [ 'o_shop_ext' => $id ] );
    }

    public function getBlockedLabelAttribute() {
        return $this->o_blocked === 0 ? 'NO' : 'SI';
    }

    public function getCancelledLabelAttribute() {
        return $this->o_cancelled == null ? 'NO' : 'SI';
    }

    public static function getTableColumns() {
    list($cols, $values) = array_divide((new static)->first()->toArray());
    return $cols;
}
}
