<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get( '/home', 'HomeController@index' )->middleware( [ '\App\Http\Middleware\CheckAdmin' ] )->name( 'home' );

Route::get( '/db_backup', 'AdminsController@download_backup' )->middleware( [ '\App\Http\Middleware\CheckAdmin' ] )->name( 'db_backup' );
Route::get( '/db_partial_backup_csv', 'AdminsController@download_partial_backup_csv' )->middleware( [ '\App\Http\Middleware\CheckAdmin' ] )->name( 'db_partial_backup_csv' );


Route::group(
    [
        'prefix'    => 'users',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( 'administrators', 'UsersController@administrators' )->name( 'administrators' );
        Route::get( 'customers', 'UsersController@customers' )->name( 'customers' );
        Route::post( 'customers', 'UsersController@customersearch' )->name( 'customers-search' );
        Route::get( 'supervisors', 'UsersController@supervisors' )->name( 'supervisors' );
        Route::get( 'shop-owners', 'UsersController@commerciants' )->name( 'shop-owners' );
        Route::post( 'shop-owners', 'UsersController@commerciantsearch' )->name( 'commerciants-search' );
        Route::get( 'details/{id}', 'UsersController@details' )->name( 'user-details' );
        Route::get( 'status/{id}/{status}', 'UsersController@status' )->name( 'user-status' );
        Route::get( 'confirmed/{id}/{confirmed}', 'UsersController@confirmed' )->name( 'user-confirmed' );
        Route::get( 'delete-customer/{id}', 'UsersController@deleteCustomer' )->name( 'delete-customer' );
    }
);

Route::group(
    [
        'prefix'    => 'customers',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( 'favs/{id}', 'CustomersController@customerFavs' )->name( 'customer-favs' );
        Route::get( 'edit/{id}', 'CustomersController@edit' )->name( 'edit-customer' );
        Route::post( 'update', 'CustomersController@update' )->name( 'update-customer' );
        Route::get( 'export', 'CustomersController@export' )->name( 'export-customers' );
    }
);

Route::group(
    [
        'prefix'    => 'shop-owners',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( 'edit/{id}', 'CommerciantsController@edit' )->name( 'edit-shop-owner' );
        Route::post( 'update', 'CommerciantsController@update' )->name( 'update-shop-owner' );
        Route::get( 'offers-trade/{id}', 'CommerciantsController@offersTrade' )->name( 'offers-trade' );
        Route::post( 'offers-trade-save/', 'CommerciantsController@offersTradeSave' )->name( 'offers-trade-save' );
        Route::get( 'delete-all-data/{id}', 'CommerciantsController@deleteAllData' )->name( 'delete-shop-owner-data' );
        Route::get( 'export', 'CommerciantsController@export' )->name( 'export-shop-owner' );

    }
);

Route::group(
    [
        'prefix'    => 'supervisors',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( 'new', 'SupervisorsController@new' )->name( 'add-supervisor' );
        Route::post( 'create', 'SupervisorsController@create' )->name( 'create-supervisor' );
        Route::get( 'edit/{id}', 'SupervisorsController@edit' )->name( 'edit-supervisor' );
        Route::post( 'update', 'SupervisorsController@update' )->name( 'update-supervisor' );
        Route::get( 'remove-from-mall/{id}', 'SupervisorsController@removeFromMall' )->name( 'remove-from-mall' );
        Route::get( 'delete-all-data/{id}', 'SupervisorsController@deleteAllData' )->name( 'delete-supervisor-data' );
    }
);

Route::group(
    [
        'prefix'    => 'admins',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( 'new', 'AdminsController@new' )->name( 'add-admin' );
        Route::post( 'create', 'AdminsController@create' )->name( 'create-admin' );
        Route::get( 'edit/{id}', 'AdminsController@edit' )->name( 'edit-admin' );
        Route::post( 'update', 'AdminsController@update' )->name( 'update-admin' );

    }
);

Route::group(
    [
        'prefix'    => 'shops',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( '', 'ShopsController@index' )->name( 'all-shops' );
        Route::get( 'malls/{id}', 'ShopsController@catenacommerciale' )->name( 'all-shops-catenacommerciale' );
        Route::get( 'ccenters/{id}', 'ShopsController@centrocommerciale' )->name( 'all-shops-centrocommerciale' );
        Route::get( 'details/{id}', 'ShopsController@details' )->name( 'shop-details' );
        Route::get( 'status/{id}/{status}', 'ShopsController@status' )->name( 'shop-status' );
        Route::get( 'edit/{id}', 'ShopsController@edit' )->name( 'edit-shop' );
        Route::post( 'update', 'ShopsController@update' )->name( 'update-shop' );
        Route::get( 'offers/{id}', 'ShopsController@offers' )->name( 'shop-offers' );
        Route::get( 'export', 'ShopsController@export' )->name( 'export-shops' );

        Route::get( 'new', 'ShopsController@new' )->name( 'add-shop' );
        Route::post( 'create', 'ShopsController@create' )->name( 'create-shop' );

        Route::get( 'upload-image/{id}', 'ShopsController@uploadImage' )->name( 'shop-upload-image' );
        Route::post( 'save-image', 'ShopsController@saveImage' )->name( 'shop-save-image' );

        Route::get( 'delete-all-data/{id}', 'ShopsController@deleteAllData' )->name( 'delete-shop-data' );
    }
);

Route::group(
    [
        'prefix'    => 'malls', //CATENE COMMERCIALI
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( '', 'MallsController@index' )->name( 'all-malls' );
        Route::get( 'details/{id}', 'MallsController@details' )->name( 'mall-details' );
        Route::get( 'edit/{id}', 'MallsController@edit' )->name( 'edit-mall' );
        Route::get( 'delete/{id}', 'MallsController@delete' )->name( 'delete-mall' );
        Route::post( 'update', 'MallsController@update' )->name( 'update-mall' );
        Route::get( 'remove-supervisor/{ccid}/{sid}', 'MallsController@removeSupervisor' )->name( 'remove-supervisor' );
        Route::get( 'export', 'MallsController@export' )->name( 'export-malls' );

        Route::get( 'new', 'MallsController@new' )->name( 'add-mall' );
        Route::post( 'create', 'MallsController@create' )->name( 'create-mall' );

    }
);

Route::group(
    [
        'prefix'    => 'offers',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( '', 'OffersController@index' )->name( 'all-offers' );
        Route::post( '', 'OffersController@offersearch' )->name( 'offers-search' );

        Route::get( 'details/{id}', 'OffersController@details' )->name( 'offers-details' );

        Route::get( 'status/{id}/{status}', 'OffersController@status' )->name( 'offer-status' );
        Route::get( 'cancel/{id}/{status}', 'OffersController@cancel' )->name( 'offer-cancel' );

        Route::get( 'edit/{id}', 'OffersController@edit' )->name( 'edit-offer' );
        Route::post( 'update', 'OffersController@update' )->name( 'update-offer' );
        Route::get( 'export', 'OffersController@export' )->name( 'export-offers' );

        Route::get( 'upload-image/{id}', 'OffersController@uploadImage' )->name( 'offer-upload-image' );
        Route::post( 'save-image', 'OffersController@saveImage' )->name( 'offer-save-image' );
    }
);

Route::group(
    [
        'prefix'    => 'centers',
        // 'namespace' => 'App\Http\Controllers'
    ],
    function () {
        Route::get( '', 'CCentersController@index' )->name( 'all-centers' );
        Route::get( 'details/{id}', 'CCentersController@details' )->name( 'center-details' );
        Route::get( 'edit/{id}', 'CCentersController@edit' )->name( 'edit-center' );
        Route::get( 'delete/{id}', 'CCentersController@delete' )->name( 'delete-center' );
        Route::post( 'update', 'CCentersController@update' )->name( 'update-center' );
        Route::get( 'new', 'CCentersController@new' )->name( 'add-center' );
        Route::post( 'create', 'CCentersController@create' )->name( 'create-center' );
        Route::get( 'export', 'CCentersController@export' )->name( 'export-centers' );
    }
);
